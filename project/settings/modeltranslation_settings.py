from django.utils.translation import ugettext_lazy as _

LANGUAGES = (
    ('it', _('Italian')),
    ('en', _('English')),
)

MODELTRANSLATION_DEFAULT_LANGUAGE = 'en'

MODELTRANSLATION_DEBUG = True