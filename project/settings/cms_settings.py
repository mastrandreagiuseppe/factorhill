from django.utils.translation import ugettext_lazy as _

CMS_TEMPLATES = (
    ('cms_templates/home.html', _('Home')),
    ('cms_templates/contacts.html', _('Contacts')),
    ('cms_templates/about_us.html', _('About us')),
    ('cms_templates/generic.html', _('Generic')),
)

CMS_TEMPLATE_INHERITANCE = True


CMS_PLACEHOLDER_CONF = {
    'Gallery': {
        'plugins': ['GenericGalleryPlugin', ],
        'text_only_plugins': ['TextPlugin'],
        'extra_context': {"width":640},
        'name': _("Gallery"),
        'language_fallback': True,
        #'default_plugins': [
        #    {
        #        'plugin_type': 'TextPlugin',
        #        'values': {
        #            'body':'<p>Lorem ipsum dolor sit amet...</p>',
        #        },
        #    },
        #],
        #'child_classes': {
        #    'TextPlugin': ['PicturePlugin', 'LinkPlugin'],
        #},
        'child_classes': {
            'GenericGalleryPlugin': ['HomepageGallerySlidePlugin'],
        },
    },
    'Lista Extra':{
        'plugins': ['ExtraProjectInfo'],
        'name': _("Extra Info"),
    },
    'Top container':{
        'plugins': ['GenericGalleryPlugin', 'ImagePlugin', 'BackgroundSizeContainImg'],
        'text_only_plugins': ['TextPlugin'],
        'extra_context': {"width":640},
        'name': _("Top Container"),
        'language_fallback': True,
        #'default_plugins': [
        #    {
        #        'plugin_type': 'TextPlugin',
        #        'values': {
        #            'body':'<p>Lorem ipsum dolor sit amet...</p>',
        #        },
        #    },
        #],
        #'child_classes': {
        #    'TextPlugin': ['PicturePlugin', 'LinkPlugin'],
        #},
        'child_classes': {
            'GenericGalleryPlugin': ['HomepageGallerySlidePlugin'],
        },
    },
    'Bio': {
        'plugins': ['GenericTextContentPlugin', 'GenericPageGalleryPlugin',],
        'text_only_plugins': ['TextPlugin'],
        'extra_context': {"width":640},
        'name': _("Bio"),
        'language_fallback': True,
        #'default_plugins': [
        #    {
        #        'plugin_type': 'TextPlugin',
        #        'values': {
        #            'body':'<p>Lorem ipsum dolor sit amet...</p>',
        #        },
        #    },
        #],
        #'child_classes': {
        #    'TextPlugin': ['PicturePlugin', 'LinkPlugin'],
        #},
        'child_classes': {
            'GenericPageGalleryPlugin': ['GenericPageGallerySlidePlugin'],
        },
    },
    'Content':{
        'plugins': ['GenericTextContentPlugin', 'GenericPageGalleryPlugin','FacebookEmbedVideoPlugin'],
        'text_only_plugins': ['TextPlugin'],
        'extra_context': {"width":640},
        'name': _("Content"),
        'language_fallback': True,
        #'default_plugins': [
        #    {
        #        'plugin_type': 'TextPlugin',
        #        'values': {
        #            'body':'<p>Lorem ipsum dolor sit amet...</p>',
        #        },
        #    },
        #],
        #'child_classes': {
        #    'TextPlugin': ['PicturePlugin', 'LinkPlugin'],
        #},
        'child_classes': {
            'GenericPageGalleryPlugin': ['GenericPageGallerySlidePlugin'],
        },
    },
    'Facebook App':{
        'plugins': ['FacebookPlugin', ''],
        'name': _("Facebook Plugin"),
        'language_fallback': True,
    },
    'Social':{
        'plugins': ['SocialLinkPlugin', ''],
        'name': _("Social Link"),
        'language_fallback': True,
    },
    #'right-column': {
    #    "plugins": ['TeaserPlugin', 'LinkPlugin'],
    #    "extra_context": {"width": 280},
    #    'name': _("Right Column"),
    #    'limits': {
    #        'global': 2,
    #        'TeaserPlugin': 1,
    #        'LinkPlugin': 1,
    #    },
    #    'plugin_modules': {
    #        'LinkPlugin': 'Extra',
    #    },
    #    'plugin_labels': {
    #        'LinkPlugin': 'Add a link',
    #    },
    #},
    #'base.html content': {
    #    "plugins": ['TextPlugin', 'PicturePlugin', 'TeaserPlugin'],
    #    'inherit': 'content',
    #},
}



'''
CMS_TEMPLATES_DIR: {
    1: '/absolute/path/for/site/1/',
    2: '/absolute/path/for/site/2/',
}

CMS_PLACEHOLDER_CONF = {
    None: {
        "plugins": ['TextPlugin'],
        'excluded_plugins': ['InheritPlugin'],
    }
    'content': {
        'plugins': ['TextPlugin', 'PicturePlugin'],
        'text_only_plugins': ['LinkPlugin'],
        'extra_context': {"width":640},
        'name': _("Content"),
        'language_fallback': True,
        'default_plugins': [
            {
                'plugin_type': 'TextPlugin',
                'values': {
                    'body':'<p>Lorem ipsum dolor sit amet...</p>',
                },
            },
        ],
        'child_classes': {
            'TextPlugin': ['PicturePlugin', 'LinkPlugin'],
        },
        'parent_classes': {
            'LinkPlugin': ['TextPlugin'],
        },
    },
    'right-column': {
        "plugins": ['TeaserPlugin', 'LinkPlugin'],
        "extra_context": {"width": 280},
        'name': _("Right Column"),
        'limits': {
            'global': 2,
            'TeaserPlugin': 1,
            'LinkPlugin': 1,
        },
        'plugin_modules': {
            'LinkPlugin': 'Extra',
        },
        'plugin_labels': {
            'LinkPlugin': 'Add a link',
        },
    },
    'base.html content': {
        "plugins": ['TextPlugin', 'PicturePlugin', 'TeaserPlugin'],
        'inherit': 'content',
    },
}

CMS_MEDIA_PATH = 'cms/'
CMS_MEDIA_ROOT = MEDIA_ROOT + CMS_MEDIA_PATH
CMS_MEDIA_URL = MEDIA_URL + CMS_MEDIA_PATH

CMS_PAGE_MEDIA_PATH = 'cms_page_media/'

'''

CMS_PLUGIN_CONTEXT_PROCESSORS = []
CMS_PLUGIN_PROCESSORS = []
CMS_APPHOOKS = (
    #'myapp.cms_app.MyApp',
    #'otherapp.cms_app.MyFancyApp',
    #'sampleapp.cms_app.SampleApp',
)

CKEDITOR_SETTINGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['FilerImage']
        ],
        'extraPlugins': 'filerimage',
        'removePlugins': 'image'
    },
}

CMS_LANGUAGES = {
    1: [
        {
            'code': 'it',
            'name': _('Italian'),
            'fallbacks': ['en'],
            'public': True,
            'hide_untranslated': True,
            'redirect_on_fallback':False,
        },
        {
            'code': 'en',
            'name': _('English'),
            'public': True,
            'hide_untranslated': True,
            'redirect_on_fallback': False,
        },
    ],
    'default': {
        'fallbacks': ['it', 'en', ],
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': True,
    }
}
