from django.utils.translation import ugettext_lazy as _

ADMIN_TOOLS_INDEX_DASHBOARD = 'fluent_dashboard.dashboard.FluentIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'fluent_dashboard.dashboard.FluentAppIndexDashboard'
ADMIN_TOOLS_MENU = 'fluent_dashboard.menu.FluentMenu'

FLUENT_DASHBOARD_ICON_THEME = 'flaticons'

#Per sapere quali sono i path esatti delle app da usare sono andato su fluent_dashboard/modules.py e ho fatto la print di key alla linea 176
FLUENT_DASHBOARD_APP_ICONS = {
    'admincms/page': 'file.png',
    'admincms/staticplaceholder': 'placeholder.png',
    'adminauth/user':  'user-3.png',
    'adminauth/group': 'users.png',
    'adminsites/site': 'star-1.png',
    
    'admincorporate/news': 'newspaper.png',
    'admincorporate/newscategory': 'notebook-1.png',

    'admincorporate/event': 'internet.png',
    'admincorporate/eventcategory': 'notebook-2.png',

    'admincorporate/alumn': 'home-2.png',
    'admincorporate/mailchimplist': 'mailchimp.png',

    'admincorporate/videoentry': 'video-player-1.png',
    'admincorporate/pressrelease': 'notepad.png',

    'adminphotologue/photo': 'picture.png',
    'adminphotologue/gallery': 'photos.png',
    'admincorporate/sentemail': 'paper-plane-1.png',
    
    'adminportfolio/portfolioitem': 'home.png',
    'adminportfolio/portfoliocategory': 'notebook-3.png'
        
    
    # ...
}

FLUENT_DASHBOARD_DEFAULT_ICON = 'error.png'

FLUENT_DASHBOARD_DEFAULT_MODULE = 'admin_tools.dashboard.modules.AppList'

# Application grouping:

FLUENT_DASHBOARD_APP_GROUPS = (
    (_('CMS'), {
        'models': (
            '*cms*.*',
            'fiber.*',
        ),
        'module': 'CmsAppIconList',
        'collapsible': False,
    }),
    (_('Corporate'), {
        'models': (
            'project.apps.corporate.models.*',
        ),
    }),
    (_('Portfolio'), {
        'models': (
            'project.apps.portfolio.models.*',
        ),
    }),
    (_('Administration'), {
        'models': (
            'django.contrib.auth.*',
            'django.contrib.sites.*',
            'google_analytics.*',
            'registration.*',
        ),
    }),

    (_('Images'), {
        'models': (
            'photologue.models.Photo',
            'photologue.models.Gallery'
        ),
    })
)


# CMS integration:

FLUENT_DASHBOARD_CMS_PAGE_MODEL = ('cms', 'page')

FLUENT_DASHBOARD_CMS_APP_NAMES = (
    '*cms*',  # wildcard match; DjangoCMS, FeinCMS
    'fiber',  # Django-Fiber
)

FLUENT_DASHBOARD_CMS_MODEL_ORDER = {
    'page': 1,
    'object': 2,
    'layout': 3,
    'content': 4,
    'file': 5,
    'site': 99
}