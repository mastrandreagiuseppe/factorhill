def splitter(tag_string):
    import re
    ar = []
    for s in tag_string.split(','):
        val = re.sub(r'"+', '', s).strip()
        if val is not '':
            ar.append(val)
    return ar


TAGGIT_TAGS_FROM_STRING = 'project.settings.taggit_settings.splitter'

'''
def comma_joiner(tags):
    names = []
    for tag in tags:
        name = tag.name
        if ',' in name or ' ' in name:
            names.append('"%s"' % name)
        else:
            names.append(name)
    return names


TAGGIT_STRING_FROM_TAGS = 'project.settings.taggit_settings.comma_joiner'''