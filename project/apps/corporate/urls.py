# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import MessageCreateAndSend, SubscribeToListView, GetAccessToken

from .api.views import ApiGalleryDetailView, ApiGalleryListView, ApiPhotoDetailView, ApiPhotoListView, ApiVideoEntriesList


urlpatterns = [
	url(r'^subscribe-to-list$', SubscribeToListView.as_view(), name='mailchimp-list-subscribe'),
	url(r'^send-mail$', MessageCreateAndSend.as_view(), name='send-message'),

	url(r'^get-auth-token$', GetAccessToken.as_view(), name='get_auth_token'),

	url(r'^api/galleries/(?P<pk>\d+)$', ApiGalleryDetailView.as_view(), name='api-gallery-detail'),
    url(r'^api/galleries/$', ApiGalleryListView.as_view(), name='api-galleries-list'),
    
    url(r'^api/photos/(?P<pk>\d+)$', ApiPhotoDetailView.as_view(), name='api-photo-detail'),
    url(r'^api/photos/$', ApiPhotoListView.as_view(), name='api-photos-list'),

    url(r'^api/videoentries/$', ApiVideoEntriesList.as_view(), name='api-videoentries-list'),
]