# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin
from tinymce.widgets import TinyMCE

from .models import NewsCategory, News, Event, EventCategory, GenericContentPluginModel, PressRelease, SocialLinkPlugin

class NewsAdminForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = News
        fields = '__all__'

class EventForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = Event
        fields = '__all__'

class NewsCategoryForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = NewsCategory
        fields = '__all__'

class EventCategoryForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = EventCategory
        fields = '__all__'

class PressReleaseForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(required=False)
    class Meta:
        model = PressRelease
        fields = '__all__'

class BioPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'content', ]

class StaffPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'cover_img', 'content', 'fb_link', 'twitter_link', 'linkedin_link', 'instagram_link', 'email']

class ImagePluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'cover_img', 'link' ]

class EmptyRowForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', ]

class VideoPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'column_width' ,'content', 'cover_img', 'yt_link' ]

class GenericContentPluginForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'gallery' ,'content', ]

class SubscribeToListForm(forms.Form):
    mailchimp_list_id = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    name = forms.CharField(required=True)

class FacebookAppForm(forms.Form):
    client_id = forms.CharField(required=True)
    client_secret = forms.CharField(required=True)
    fb_exchange_token = forms.CharField(required=True)

class GenericTextPluginForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'content', ]

class ExtraProjectInfoForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'content', 'cover_img']

class MultimediaBioPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'cover_img', 'yt_link', ]

class HomepageGallerySlidePluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['icon', 'title', 'subtitle', 'excerpt', 'cover_img', 'content_img', 'bg_color', 'text_color', 'full_screen', 'yt_link', 'link', 'link_text', 'auto_play',]

class GenericGalleryForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'duration', 'auto_play', 'full_screen', ]

class GenericPageGalleryForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'duration', 'auto_play', 'gallery']

class GenericPageGallerySlidePluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'cover_img', 'text_color', 'link', ]

class SocialLinkPluginForm(forms.ModelForm):
    class Meta:
        model = SocialLinkPlugin
        fields = ['social', 'link',]
