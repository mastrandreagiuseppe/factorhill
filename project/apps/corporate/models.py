# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.core.urlresolvers import reverse_lazy, reverse

from django.utils.translation import ugettext_lazy as _

from colorfield.fields import ColorField
from cms.models import CMSPlugin
from taggit.managers import TaggableManager

from photologue.models import Gallery, Photo

from project.apps.base.models import AdvancedCategory, AdvancedEntryModel
from project.apps.base.mixins import OrderableModelMixin

SOCIAL_CHOICES = (
    ('facebook', 'Facebook'),
    ('twitter', 'Twitter'),
    ('youtube-square', 'Youtube'),
    ('instagram', 'Instagram'),
    ('linkedin', 'Linkedin'),
    ('behance', 'Behance'),
    ('dribbble', 'Dribbble'),
    ('vk', 'VK'),
)


class NewsCategory(AdvancedCategory):
    cover_img = models.ForeignKey(Photo, related_name='news_category', blank=True, null=True, verbose_name=_('Cover'))
    class Meta(AdvancedCategory.Meta):
        verbose_name = _('News category')
        verbose_name_plural = _('News categories')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('news:news-category-list', kwargs={'slug':self.slug})

class EventCategory(AdvancedCategory):
    cover_img = models.ForeignKey(Photo, related_name='event_category', blank=True, null=True, verbose_name=_('Cover'))
    fb_link = models.CharField(_("FB Link"), max_length=512, blank=True, null=True)
    class Meta(AdvancedCategory.Meta):
        verbose_name = _('Events Category')
        verbose_name_plural = _('Events Categories')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('events:events-category-list', kwargs={'slug':self.slug})

class News(AdvancedEntryModel, OrderableModelMixin):
    gallery = models.ForeignKey(Gallery, related_name='news', blank=True, null=True, verbose_name=_("Gallery"))
    cover_img = models.ForeignKey(Photo, related_name='news', blank=True, null=True, verbose_name=_('Cover'))
    category = models.ForeignKey(NewsCategory, related_name='news', blank=True, null=True, verbose_name=_('Category'))
    # This is the important bit - where we add in the tags.
    tags = TaggableManager(blank=True, verbose_name=_("Tags"))
    fb_link = models.CharField(_("FB Link"), max_length=512, blank=True, null=True)
    videos = models.ManyToManyField(
        'VideoEntry',
        related_name="news",
        related_query_name="%(app_label)s_%(class)ss",
        verbose_name=_('related_videos'),
        blank=True
    )

    class Meta(AdvancedEntryModel.Meta):
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ['order',]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('news:news-detail', kwargs={'slug':self.slug})

class PressRelease(AdvancedEntryModel, OrderableModelMixin):
    downloadable_link = models.FileField(_('Allegato'), upload_to='uploads/downloadable/', blank=True, null=True)
    cover_img = models.ForeignKey(Photo, related_name='pressrelease', blank=True, null=True, verbose_name=_('Cover'))
    # This is the important bit - where we add in the tags.
    tags = TaggableManager(blank=True, verbose_name=_("Events Type"))
    fb_link = models.CharField(_("FB Link"), max_length=512, blank=True, null=True)
    videos = models.ManyToManyField(
        'VideoEntry',
        related_name="press_releases",
        related_query_name="%(app_label)s_%(class)ss",
        verbose_name=_('related_videos'),
        blank=True
    )

    class Meta(AdvancedEntryModel.Meta):
        verbose_name = _('Press Release')
        verbose_name_plural = _('Press Releases')
        ordering = ['order',]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('press:pressrelease-detail', kwargs={'slug':self.slug})

class VideoEntry(AdvancedEntryModel, OrderableModelMixin):
    cover_img = models.ForeignKey(Photo, related_name='video', blank=True, null=True, verbose_name=_('Cover'))
    link = models.URLField(_('Link'), blank=True, null=True)

    class Meta(AdvancedEntryModel.Meta):
        verbose_name = _('Video Entry')
        verbose_name_plural = _('Video Entries')
        ordering = ['order',]

class Event(AdvancedEntryModel, OrderableModelMixin):
    gallery = models.ForeignKey(Gallery, related_name='event', blank=True, null=True, verbose_name=_("Gallery"))
    cover_img = models.ForeignKey(Photo, related_name='event', blank=True, null=True, verbose_name=_('Cover'))
    category = models.ForeignKey(EventCategory, related_name='news', blank=True, null=True, verbose_name=_('Category'))
    event_start_date = models.DateTimeField(_("Event start date"), blank=True, null=True)
    event_end_date = models.DateTimeField(_("Event end date"), blank=True, null=True)
    event_link = models.CharField(_("FB Link"), max_length=512, blank=True, null=True)
    tags = TaggableManager(blank=True, verbose_name=_("Tags"))
    videos = models.ManyToManyField(
        'VideoEntry',
        related_name="events",
        related_query_name="%(app_label)s_%(class)ss",
        verbose_name=_('related_videos'),
        blank=True
    )

    class Meta(AdvancedEntryModel.Meta):
        verbose_name = _('Event')
        verbose_name_plural = _('Events')
        ordering = ['order',]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('events:event-detail', kwargs={'slug':self.slug})

class SentEmail(models.Model):
    sender_email = models.EmailField(_("Sender Email"))
    created = models.DateTimeField(_("modified at"), auto_now=True)
    subject = models.CharField(_("Subject"), max_length=255, blank=True, null=True)
    body = models.TextField(_("Body"))
    phone = models.TextField(_("Telefono"), blank=True, null=True)

    class Meta:
        verbose_name = _('Sent Email')
        verbose_name_plural = _('Sent Emails')
        ordering = ['-created']
        get_latest_by = '-created'

    def __unicode__(self):
        return self.subject

class ContactPlugin(CMSPlugin, OrderableModelMixin):
    address = models.CharField(_('Address'), max_length=255, blank=True, null=True)
    city = models.CharField(_('City'), max_length=255, blank=True, null=True)
    zipcode = models.CharField(_('zipcode'), max_length=255, blank=True, null=True)
    telephone_1 = models.CharField(_('telephone 1'), max_length=255, blank=True, null=True)
    telephone_2 = models.CharField(_('telephone 2'), max_length=255, blank=True, null=True)
    skype = models.CharField(_('skype'), max_length=255, blank=True, null=True)
    email = models.CharField(_('email'), max_length=255, blank=True, null=True)
    fax = models.CharField(_('fax'), max_length=255, blank=True, null=True)
    fb_link = models.URLField(_('Facebook'), blank=True, null=True)
    twitter_link = models.URLField(_('Twitter'), blank=True, null=True)
    linkedin_link = models.URLField(_('Linkedin'), blank=True, null=True)
    instagram_link = models.URLField(_('Instagram'), blank=True, null=True)
    cover_img = models.ImageField(upload_to='uploads/generic', verbose_name=_("Cover"), blank=True, null=True)
    yt_link = models.URLField(_('Youtube'), blank=True, null=True)

    class Meta:
        verbose_name = _('Contatto')
        verbose_name_plural = _('Contatti')

    def __unicode__(self):
        return self.address

class MailchimpList(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    mailchimp_id = models.CharField(_('Mailchimp id'), max_length=255)
    class Meta:
        verbose_name = _('Mailing list mailchimp')
        verbose_name_plural = _('Mailing list mailchimp')

    def __unicode__(self):
        return self.name

class FacebookApp(models.Model):
    app_name = models.CharField(_('App Name'), max_length=255, blank=True, null=True)
    app_id = models.CharField(_('App Id'), max_length=255, blank=True, null=True)
    secret = models.CharField(_('Secret'), max_length=255, blank=True, null=True)
    app_token = models.CharField(_('Access Token'), max_length=255, blank=True, null=True)
    long_term_access_token = models.CharField(_('Long Term Access Token'), max_length=255, blank=True, null=True)
    long_term_access_token_expiring = models.DateTimeField(_("Long term access token expiring date"), blank=True, null=True)

    class Meta:
        verbose_name = _('Facebook App')
        verbose_name_plural = _('Facebook Apps')

    def __unicode__(self):
        return self.app_name

class FacebookAppPlugin(CMSPlugin):
    facebook_app = models.ForeignKey(
        FacebookApp,
        related_name = 'facebook_app_plugin'
    )

    def __unicode__(self):
        return self.facebook_app.app_name


class SocialLinkPlugin(CMSPlugin, OrderableModelMixin):
    social = models.CharField(_("Social"), max_length=155, blank=True, null=True, choices=SOCIAL_CHOICES)
    link = models.URLField(_('Page Url'), blank=True, null=True)

    def __unicode__(self):
        return self.link

class GenericContentPluginModel(CMSPlugin, OrderableModelMixin):
    created = models.DateTimeField(_("created at"), auto_now_add=True, blank=True, null=True)
    column_width = models.IntegerField(_("Column Width"), blank=True, null=True, default=3)
    modified = models.DateTimeField(_("modified at"), auto_now=True, blank=True, null=True)
    created_by = models.CharField(_('created by'), max_length=155, blank=True, editable=False)
    modified_by = models.CharField(_('modified by'), max_length=155, blank=True, editable=False)
    title = models.CharField(_("title"), max_length=155, blank=True, null=True)
    slug = models.SlugField(_('slug'), max_length=155, blank=True, editable=False)
    subtitle = models.CharField(_("subtitle"), max_length=155, blank=True)
    content = models.TextField(_("content"), blank=True)
    excerpt = models.TextField(_("abstract"), blank=True)
    full_screen = models.BooleanField(_("Full Screen"), default=False)
    link = models.URLField(_('Link'), blank=True, null=True)
    link_text = models.CharField(_("Link Text"), max_length=155, blank=True)
    fb_link = models.URLField(_('Facebook'), blank=True, null=True)
    twitter_link = models.URLField(_('Twitter'), blank=True, null=True)
    linkedin_link = models.URLField(_('Linkedin'), blank=True, null=True)
    instagram_link = models.URLField(_('Instagram'), blank=True, null=True)
    yt_link = models.URLField(_('Youtube'), blank=True, null=True)
    email = models.EmailField(_('Email'), blank=True, null=True)
    cover_img = models.ImageField(upload_to='uploads/generic', verbose_name=_("Cover/Background image"), blank=True, null=True)
    gallery = models.ForeignKey(Gallery, related_name='generic_content', blank=True, null=True, verbose_name=_("Gallery"))
    audio_file = models.FileField(_('Audio'), upload_to='uploads/audio/', blank=True, null=True)
    icon = models.CharField(_('icon'), max_length=155, blank=True, null=True)
    bg_color = ColorField(_('Bg Color'), default='#FF0000')
    text_color = ColorField(_('Text Color'), default='#ffffff')
    auto_play = models.BooleanField(_("Auto Play (Just for yt_link)"), default=False)
    duration = models.IntegerField(_("Slides speed (in ms)"), default=3000)
    auto_play = models.BooleanField(_("Autoplay Slider"), default=True)
    content_img = models.ImageField(upload_to='uploads/generic', verbose_name=_("Content Image"), blank=True, null=True)

    def copy_relations(self, oldinstance):
        self.cover_img = oldinstance.cover_img
        self.gallery = oldinstance.gallery

    def __unicode__(self):
        return self.title
