# -*- coding: utf-8 -*-
""" ENTRIES MODULE > MANAGERS
    @author: Domenico Lamparelli <domenico@frankhood.it> 
    @author: Nicola Mosca <nico@frankhood.it>
"""
from __future__ import absolute_import, unicode_literals

import datetime
import logging

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.db.models import Count
from django.utils import timezone

from django.contrib.sites.managers import CurrentSiteManager

from modeltranslation.manager import MultilingualManager

from project.apps.base.managers import PublishedCategoryManager

logger = logging.getLogger('project')


# in models --> published_objects = PublishedCategoryManager()
# Non funziona questo manager... lo stesso filtro invece funziona se filtro lq queryset nelle viste... probabilmente dipende dal fatto che non individua il current language?
class NotBlankSlugPublishedManager(MultilingualManager, PublishedCategoryManager):
    def get_queryset(self):
        """ get only the published entries """
        qs = super(NotBlankSlugPublishedManager, self).get_queryset()
        qs.exclude(slug__isnull=True).exclude(slug='')
        return qs
