# -*- coding: utf-8 -*-

from rest_framework import permissions, serializers
from ..models import VideoEntry
from .gallery import PhotoSerializer

from . import BASE_DETAIL_FIELDS, BASE_CATEGORY_FIELDS


class VideoEntrySerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    cover_img = PhotoSerializer(read_only=True)
    absolute_url = serializers.URLField(source='get_absolute_url', read_only=True)
    class Meta:
        model = VideoEntry
        fields = (
			'id', 
			'title', 
			'subtitle',
			'slug', 
			'order',
			'cover_img', 
			'excerpt',
			'content',
			'status', 
			'publication_start', 
			'publication_end' ,
			'absolute_url',
			'link',
		)