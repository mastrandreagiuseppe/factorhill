
BASE_CATEGORY_FIELDS = (
    'id',
    'name',
    'slug',
    'css_class',
    'order',
    'excerpt',
    'content',
    'cover_img' 
)

BASE_DETAIL_FIELDS = ( 
    'id', 
    'title', 
    'subtitle',
    'slug', 
    'order',
    'gallery', 
    'cover_img', 
    'excerpt',
    'content',
    'status', 
    'publication_start', 
    'publication_end' 
)

BASE_CATEGORIZED_FIELDS = BASE_DETAIL_FIELDS + ( 'category', )

