# -*- coding: utf-8 -*-

from rest_framework import permissions, serializers
from ..models import Event, EventCategory
from .gallery import GallerySerializer, PhotoSerializer
from sorl.thumbnail import get_thumbnail

from . import BASE_DETAIL_FIELDS, BASE_CATEGORY_FIELDS, BASE_CATEGORIZED_FIELDS

class EventCategorySerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    class Meta:
        model = EventCategory
        permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        fields = BASE_CATEGORY_FIELDS + ('fb_link',)

class EventSerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    category = EventCategorySerializer(read_only=True)
    gallery = GallerySerializer(read_only=True)
    #cover_img = PhotoSerializer(read_only=True)
    thumbnail = serializers.SerializerMethodField('retrieve_thumbnail')

    def retrieve_thumbnail (self, obj):
        if(obj.cover_img):
            im = get_thumbnail(obj.cover_img.image, '1920x1280', crop="center", quality=99)
            return im.url
        else:
            return ''
    
    absolute_url = serializers.URLField(source='get_absolute_url', read_only=True)

    class Meta:
        model = Event
        fields = BASE_CATEGORIZED_FIELDS + ( 'event_start_date', 'event_end_date', 'event_link', 'thumbnail' , 'absolute_url')
