# -*- coding: utf-8 -*-

from rest_framework import permissions, serializers
from ..models import PressRelease
from .gallery import GallerySerializer, PhotoSerializer
from .videoentries import VideoEntrySerializer

from . import BASE_DETAIL_FIELDS, BASE_CATEGORY_FIELDS

class PressReleaseSerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    videos = VideoEntrySerializer(many=True, read_only=True)
    absolute_url = serializers.URLField(source='get_absolute_url', read_only=True)
    cover_img = PhotoSerializer(read_only=True)

    
    class Meta:
        model = PressRelease
        permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        fields = (
            'id', 
            'title', 
            'subtitle',
            'slug', 
            'order',
            'cover_img',
            'excerpt',
            'content',
            'status', 
            'publication_start', 
            'publication_end' 
        ) + ('downloadable_link', 'fb_link', 'videos', 'absolute_url')
