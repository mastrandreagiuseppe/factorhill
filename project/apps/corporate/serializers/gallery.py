# -*- coding: utf-8 -*-

from photologue.models import Gallery, Photo
from rest_framework import permissions, serializers


class PhotoSerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    class Meta:
        model = Photo
        fields = ( 'id', 'title', 'slug', 'image' )

class GallerySerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    photos = PhotoSerializer(many=True, read_only=True)

    absolute_url = serializers.URLField(source='get_absolute_url', read_only=True)
    class Meta:
        model = Gallery
        fields = ( 'id', 'title', 'date_added', 'photos', 'absolute_url' )

