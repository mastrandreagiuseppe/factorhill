# -*- coding: utf-8 -*-

from rest_framework import permissions, serializers
from ..models import News, NewsCategory
from .gallery import GallerySerializer, PhotoSerializer

from . import BASE_DETAIL_FIELDS, BASE_CATEGORY_FIELDS, BASE_CATEGORIZED_FIELDS

class NewsCategorySerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    class Meta:
        model = NewsCategory
        permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        fields = BASE_CATEGORY_FIELDS

class NewsSerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    category = NewsCategorySerializer(read_only=True)
    gallery = GallerySerializer(read_only=True)
    cover_img = PhotoSerializer(read_only=True)
    absolute_url = serializers.URLField(source='get_absolute_url', read_only=True)
    class Meta:
        model = News
        fields = BASE_CATEGORIZED_FIELDS + ( 'fb_link', 'absolute_url')