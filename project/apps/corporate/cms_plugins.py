from django.utils.translation import ugettext as _
from django.contrib import admin

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import GenericContentPluginModel, ContactPlugin, FacebookAppPlugin, SocialLinkPlugin
from .forms import *


class RowPluginPublisher(CMSPluginBase):
    render_template = 'cms_templates/plugins/empty_row.html'
    name = _("Empty Row")
    model = GenericContentPluginModel  # model where plugin data are saved
    form = EmptyRowForm
    allow_children = True

class VideoPluginPublisher(CMSPluginBase):
    render_template = 'cms_templates/plugins/dummy.html'
    name = _("Video")
    model = GenericContentPluginModel  # model where plugin data are saved
    form = VideoPluginForm

class ContactPluginPublisher(CMSPluginBase):
    render_template = 'cms_templates/plugins/address_contacts.html'
    name = _("Contatti")
    model = ContactPlugin  # model where plugin data are saved

class BioPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/bio.html'
    name = _("Bio")
    model = GenericContentPluginModel  # model where plugin data are saved
    form = BioPluginForm

class BioMultimediaPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/multimedia_bio_plugin.html'
    name = _("Multimedia Bio Plugin")
    model = GenericContentPluginModel  # model where plugin data are saved
    form = MultimediaBioPluginForm

class StaffPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/staff.html'
    name = _("Staff Plugin")
    model = GenericContentPluginModel  # model where plugin data are saved
    form = StaffPluginForm

class ImagePlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/image.html'
    name = _("Image Plugin")
    model = GenericContentPluginModel  # model where plugin data are saved
    form = ImagePluginForm

class GenericContentPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/generic.html'
    name = _("Generic Page Content Plugin")
    model = GenericContentPluginModel  # model where plugin data are saved
    form = GenericContentPluginForm    

class GenericTextContentPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/generic.html'
    name = _("Generic Text Content Plugin")
    model = GenericContentPluginModel  # model where plugin data are saved
    form = GenericTextPluginForm    

class FacebookPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/fb_app.html'
    name = _("Facebook App Plugin")
    model = FacebookAppPlugin

class HomepageGallerySlidePlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/home_gallery_slide.html'
    name = _("Gallery Slide")
    model = GenericContentPluginModel
    form = HomepageGallerySlidePluginForm

class GenericGalleryPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/gallery.html'
    name = _("Gallery")
    form = GenericGalleryForm
    model = GenericContentPluginModel
    allow_children = True

class ExtraProjectInfo(CMSPluginBase):
    render_template = 'cms_templates/plugins/extra_project_info.html'
    name = _("Extra info")
    form = ExtraProjectInfoForm
    model = GenericContentPluginModel

class GenericPageGalleryPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/generic_page_gallery.html'
    name = _("Internal page gallery")
    form = GenericPageGalleryForm
    model = GenericContentPluginModel
    allow_children = True

class GenericPageGallerySlidePlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/generic_page_gallery_slide.html'
    name = _("Internal Gallery Slide")
    model = GenericContentPluginModel
    form = GenericPageGallerySlidePluginForm

class SocialLinkPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/social_link.html'
    name = _("Social Link")
    model = SocialLinkPlugin
    form = SocialLinkPluginForm    

class BackgroundSizeContainImg(CMSPluginBase):
    render_template = 'cms_templates/plugins/background_size_contain_img.html'
    name = _("Responsive Fullscreen image")
    model = GenericContentPluginModel
    form = ImagePluginForm   

class FacebookEmbedVideoPlugin(CMSPluginBase):
    render_template = 'cms_templates/plugins/fb_embed_video.html'
    name = _("Facebook Video Embed")
    model = GenericContentPluginModel
    form = ImagePluginForm    



plugin_pool.register_plugin(RowPluginPublisher)
plugin_pool.register_plugin(VideoPluginPublisher)
plugin_pool.register_plugin(ContactPluginPublisher)
#plugin_pool.register_plugin(BioPlugin)
plugin_pool.register_plugin(BioMultimediaPlugin)
plugin_pool.register_plugin(StaffPlugin)
plugin_pool.register_plugin(ImagePlugin)
plugin_pool.register_plugin(GenericContentPlugin)
plugin_pool.register_plugin(GenericTextContentPlugin)
plugin_pool.register_plugin(FacebookPlugin)
plugin_pool.register_plugin(HomepageGallerySlidePlugin)
plugin_pool.register_plugin(GenericGalleryPlugin)
plugin_pool.register_plugin(ExtraProjectInfo)
plugin_pool.register_plugin(GenericPageGalleryPlugin)
plugin_pool.register_plugin(GenericPageGallerySlidePlugin)
plugin_pool.register_plugin(SocialLinkPlugin)
plugin_pool.register_plugin(BackgroundSizeContainImg)
plugin_pool.register_plugin(FacebookEmbedVideoPlugin)
