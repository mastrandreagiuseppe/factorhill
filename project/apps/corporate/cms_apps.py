from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class EventAppHook(CMSApp):
    name = _("Events")

    app_name = 'events'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.apps.corporate.app_urls.events"]       # replace this with the path to your application's URLs module


class NewsAppHook(CMSApp):
    name = _("News")

    app_name = 'news'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.apps.corporate.app_urls.news"]       # replace this with the path to your application's URLs module 

class GalleryAppHook(CMSApp):
    name = _("Gallery")

    app_name = 'gallery'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.apps.corporate.app_urls.galleries"]       # replace this with the path to your application's URLs module 


class VideoEntryAppHook(CMSApp):
    name = _("Video Entry")

    app_name = 'videos'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.apps.corporate.app_urls.videoentries"]  

class PressReleaseAppHook(CMSApp):
    name = _("Press Release")

    app_name = 'press'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.apps.corporate.app_urls.pressrelease"]


apphook_pool.register(EventAppHook)
apphook_pool.register(NewsAppHook)
apphook_pool.register(GalleryAppHook)
apphook_pool.register(VideoEntryAppHook)
apphook_pool.register(PressReleaseAppHook)
