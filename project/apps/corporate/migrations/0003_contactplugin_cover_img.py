# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-27 22:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0002_auto_20170626_2301'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactplugin',
            name='cover_img',
            field=models.ImageField(blank=True, null=True, upload_to=b'uploads/generic', verbose_name='Cover'),
        ),
    ]
