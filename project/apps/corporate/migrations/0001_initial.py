# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-06 10:08
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import project.apps.base.fields
import taggit.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('taggit', '0002_auto_20150616_2121'),
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('photologue', '0010_auto_20160105_1307'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='corporate_contactplugin', serialize=False, to='cms.CMSPlugin')),
                ('order', models.PositiveIntegerField(blank=True, db_index=True, default=0, null=True, verbose_name='position')),
                ('address', models.CharField(blank=True, max_length=255, null=True, verbose_name='Address')),
                ('city', models.CharField(blank=True, max_length=255, null=True, verbose_name='City')),
                ('zipcode', models.CharField(blank=True, max_length=255, null=True, verbose_name='zipcode')),
                ('telephone_1', models.CharField(blank=True, max_length=255, null=True, verbose_name='telephone 1')),
                ('telephone_2', models.CharField(blank=True, max_length=255, null=True, verbose_name='telephone 2')),
                ('skype', models.CharField(blank=True, max_length=255, null=True, verbose_name='skype')),
                ('email', models.CharField(blank=True, max_length=255, null=True, verbose_name='email')),
                ('fax', models.CharField(blank=True, max_length=255, null=True, verbose_name='fax')),
                ('fb_link', models.URLField(blank=True, null=True, verbose_name='Facebook')),
                ('twitter_link', models.URLField(blank=True, null=True, verbose_name='Twitter')),
                ('linkedin_link', models.URLField(blank=True, null=True, verbose_name='Linkedin')),
                ('instagram_link', models.URLField(blank=True, null=True, verbose_name='Instagram')),
                ('yt_link', models.URLField(blank=True, null=True, verbose_name='Youtube')),
            ],
            options={
                'verbose_name': 'Contatto',
                'verbose_name_plural': 'Contatti',
            },
            bases=('cms.cmsplugin', models.Model),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'draft'), (99, 'published')], default=0, verbose_name='status')),
                ('publication_start', models.DateTimeField(blank=True, null=True, verbose_name='publication start')),
                ('publication_end', models.DateTimeField(blank=True, null=True, verbose_name='publication end')),
                ('order', models.PositiveIntegerField(blank=True, db_index=True, default=0, null=True, verbose_name='position')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='created by')),
                ('modified_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='modified by')),
                ('title', models.CharField(blank=True, max_length=155, verbose_name='title')),
                ('title_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('title_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('subtitle', models.CharField(blank=True, max_length=155, verbose_name='subtitle')),
                ('subtitle_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='subtitle')),
                ('subtitle_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='subtitle')),
                ('content', models.TextField(blank=True, verbose_name='content')),
                ('content_it', models.TextField(blank=True, null=True, verbose_name='content')),
                ('content_en', models.TextField(blank=True, null=True, verbose_name='content')),
                ('excerpt', models.TextField(blank=True, verbose_name='abstract')),
                ('excerpt_it', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('excerpt_en', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('object_id', models.PositiveIntegerField(blank=True, null=True)),
                ('event_start_date', models.DateTimeField(blank=True, null=True, verbose_name='Event start date')),
                ('event_end_date', models.DateTimeField(blank=True, null=True, verbose_name='Event end date')),
                ('event_link', models.CharField(blank=True, max_length=512, null=True, verbose_name='FB Link')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='event_written', to=settings.AUTH_USER_MODEL, verbose_name='author')),
            ],
            options={
                'get_latest_by': 'publication_start',
                'ordering': ['order'],
                'abstract': False,
                'verbose_name_plural': 'Events',
                'verbose_name': 'Event',
            },
        ),
        migrations.CreateModel(
            name='EventCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'draft'), (99, 'published')], default=0, verbose_name='status')),
                ('publication_start', models.DateTimeField(blank=True, null=True, verbose_name='publication start')),
                ('publication_end', models.DateTimeField(blank=True, null=True, verbose_name='publication end')),
                ('order', models.PositiveIntegerField(blank=True, db_index=True, default=0, null=True, verbose_name='position')),
                ('lang', models.CharField(choices=[(b'it', 'Italian'), (b'en', 'English')], max_length=5, verbose_name='language')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='created by')),
                ('modified_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='modified by')),
                ('name', models.CharField(blank=True, max_length=155, verbose_name='name')),
                ('name_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='name')),
                ('name_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='name')),
                ('css_class', models.CharField(blank=True, max_length=32, verbose_name='CSS class')),
                ('content', models.TextField(blank=True, verbose_name='content')),
                ('content_it', models.TextField(blank=True, null=True, verbose_name='content')),
                ('content_en', models.TextField(blank=True, null=True, verbose_name='content')),
                ('excerpt', models.TextField(blank=True, verbose_name='abstract')),
                ('excerpt_it', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('excerpt_en', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('fb_link', models.CharField(blank=True, max_length=512, null=True, verbose_name='FB Link')),
                ('cover_img', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='event_category', to='photologue.Photo', verbose_name='Cover')),
            ],
            options={
                'ordering': ['order'],
                'abstract': False,
                'verbose_name': 'Events Category',
                'verbose_name_plural': 'Events Categories',
            },
        ),
        migrations.CreateModel(
            name='GenericContentPluginModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='corporate_genericcontentpluginmodel', serialize=False, to='cms.CMSPlugin')),
                ('order', models.PositiveIntegerField(blank=True, db_index=True, default=0, null=True, verbose_name='position')),
                ('created', models.DateTimeField(auto_now_add=True, null=True, verbose_name='created at')),
                ('column_width', models.IntegerField(blank=True, default=3, null=True, verbose_name='Column Width')),
                ('modified', models.DateTimeField(auto_now=True, null=True, verbose_name='modified at')),
                ('created_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='created by')),
                ('modified_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='modified by')),
                ('title', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('slug', models.SlugField(blank=True, editable=False, max_length=155, verbose_name='slug')),
                ('subtitle', models.CharField(blank=True, max_length=155, verbose_name='subtitle')),
                ('content', models.TextField(blank=True, verbose_name='content')),
                ('excerpt', models.TextField(blank=True, verbose_name='abstract')),
                ('fb_link', models.URLField(blank=True, null=True, verbose_name='Facebook')),
                ('twitter_link', models.URLField(blank=True, null=True, verbose_name='Twitter')),
                ('linkedin_link', models.URLField(blank=True, null=True, verbose_name='Linkedin')),
                ('instagram_link', models.URLField(blank=True, null=True, verbose_name='Instagram')),
                ('yt_link', models.URLField(blank=True, null=True, verbose_name='Youtube')),
                ('email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Email')),
                ('cover_img', models.ImageField(blank=True, null=True, upload_to=b'uploads/generic', verbose_name='Cover')),
                ('audio_file', models.FileField(blank=True, null=True, upload_to=b'uploads/audio/', verbose_name='Audio')),
                ('gallery', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='generic_content', to='photologue.Gallery', verbose_name='Gallery')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin', models.Model),
        ),
        migrations.CreateModel(
            name='MailchimpList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('mailchimp_id', models.CharField(max_length=255, verbose_name='Mailchimp id')),
            ],
            options={
                'verbose_name': 'Mailing list mailchimp',
                'verbose_name_plural': 'Mailing list mailchimp',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'draft'), (99, 'published')], default=0, verbose_name='status')),
                ('publication_start', models.DateTimeField(blank=True, null=True, verbose_name='publication start')),
                ('publication_end', models.DateTimeField(blank=True, null=True, verbose_name='publication end')),
                ('order', models.PositiveIntegerField(blank=True, db_index=True, default=0, null=True, verbose_name='position')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='created by')),
                ('modified_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='modified by')),
                ('title', models.CharField(blank=True, max_length=155, verbose_name='title')),
                ('title_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('title_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('subtitle', models.CharField(blank=True, max_length=155, verbose_name='subtitle')),
                ('subtitle_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='subtitle')),
                ('subtitle_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='subtitle')),
                ('content', models.TextField(blank=True, verbose_name='content')),
                ('content_it', models.TextField(blank=True, null=True, verbose_name='content')),
                ('content_en', models.TextField(blank=True, null=True, verbose_name='content')),
                ('excerpt', models.TextField(blank=True, verbose_name='abstract')),
                ('excerpt_it', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('excerpt_en', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('object_id', models.PositiveIntegerField(blank=True, null=True)),
                ('fb_link', models.CharField(blank=True, max_length=512, null=True, verbose_name='FB Link')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='news_written', to=settings.AUTH_USER_MODEL, verbose_name='author')),
            ],
            options={
                'get_latest_by': 'publication_start',
                'ordering': ['order'],
                'abstract': False,
                'verbose_name_plural': 'News',
                'verbose_name': 'News',
            },
        ),
        migrations.CreateModel(
            name='NewsCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'draft'), (99, 'published')], default=0, verbose_name='status')),
                ('publication_start', models.DateTimeField(blank=True, null=True, verbose_name='publication start')),
                ('publication_end', models.DateTimeField(blank=True, null=True, verbose_name='publication end')),
                ('order', models.PositiveIntegerField(blank=True, db_index=True, default=0, null=True, verbose_name='position')),
                ('lang', models.CharField(choices=[(b'it', 'Italian'), (b'en', 'English')], max_length=5, verbose_name='language')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='created by')),
                ('modified_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='modified by')),
                ('name', models.CharField(blank=True, max_length=155, verbose_name='name')),
                ('name_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='name')),
                ('name_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='name')),
                ('css_class', models.CharField(blank=True, max_length=32, verbose_name='CSS class')),
                ('content', models.TextField(blank=True, verbose_name='content')),
                ('content_it', models.TextField(blank=True, null=True, verbose_name='content')),
                ('content_en', models.TextField(blank=True, null=True, verbose_name='content')),
                ('excerpt', models.TextField(blank=True, verbose_name='abstract')),
                ('excerpt_it', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('excerpt_en', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('cover_img', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='news_category', to='photologue.Photo', verbose_name='Cover')),
            ],
            options={
                'ordering': ['order'],
                'abstract': False,
                'verbose_name': 'News category',
                'verbose_name_plural': 'News categories',
            },
        ),
        migrations.CreateModel(
            name='PressRelease',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'draft'), (99, 'published')], default=0, verbose_name='status')),
                ('publication_start', models.DateTimeField(blank=True, null=True, verbose_name='publication start')),
                ('publication_end', models.DateTimeField(blank=True, null=True, verbose_name='publication end')),
                ('order', models.PositiveIntegerField(blank=True, db_index=True, default=0, null=True, verbose_name='position')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='created by')),
                ('modified_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='modified by')),
                ('title', models.CharField(blank=True, max_length=155, verbose_name='title')),
                ('title_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('title_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('subtitle', models.CharField(blank=True, max_length=155, verbose_name='subtitle')),
                ('subtitle_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='subtitle')),
                ('subtitle_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='subtitle')),
                ('content', models.TextField(blank=True, verbose_name='content')),
                ('content_it', models.TextField(blank=True, null=True, verbose_name='content')),
                ('content_en', models.TextField(blank=True, null=True, verbose_name='content')),
                ('excerpt', models.TextField(blank=True, verbose_name='abstract')),
                ('excerpt_it', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('excerpt_en', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('object_id', models.PositiveIntegerField(blank=True, null=True)),
                ('downloadable_link', models.FileField(blank=True, null=True, upload_to=b'uploads/downloadable/', verbose_name='Allegato')),
                ('fb_link', models.CharField(blank=True, max_length=512, null=True, verbose_name='FB Link')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pressrelease_written', to=settings.AUTH_USER_MODEL, verbose_name='author')),
                ('content_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
                ('cover_img', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pressrelease', to='photologue.Photo', verbose_name='Cover')),
                ('tags', taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Events Type')),
            ],
            options={
                'get_latest_by': 'publication_start',
                'ordering': ['order'],
                'abstract': False,
                'verbose_name_plural': 'Press Releases',
                'verbose_name': 'Press Release',
            },
        ),
        migrations.CreateModel(
            name='SentEmail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sender_email', models.EmailField(max_length=254, verbose_name='Sender Email')),
                ('created', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('subject', models.CharField(blank=True, max_length=255, null=True, verbose_name='Subject')),
                ('body', models.TextField(verbose_name='Body')),
                ('phone', models.TextField(blank=True, null=True, verbose_name='Telefono')),
            ],
            options={
                'ordering': ['-created'],
                'get_latest_by': '-created',
                'verbose_name': 'Sent Email',
                'verbose_name_plural': 'Sent Emails',
            },
        ),
        migrations.CreateModel(
            name='VideoEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(blank=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'draft'), (99, 'published')], default=0, verbose_name='status')),
                ('publication_start', models.DateTimeField(blank=True, null=True, verbose_name='publication start')),
                ('publication_end', models.DateTimeField(blank=True, null=True, verbose_name='publication end')),
                ('order', models.PositiveIntegerField(blank=True, db_index=True, default=0, null=True, verbose_name='position')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='created by')),
                ('modified_by', models.CharField(blank=True, editable=False, max_length=155, verbose_name='modified by')),
                ('title', models.CharField(blank=True, max_length=155, verbose_name='title')),
                ('title_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('title_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='title')),
                ('subtitle', models.CharField(blank=True, max_length=155, verbose_name='subtitle')),
                ('subtitle_it', models.CharField(blank=True, max_length=155, null=True, verbose_name='subtitle')),
                ('subtitle_en', models.CharField(blank=True, max_length=155, null=True, verbose_name='subtitle')),
                ('content', models.TextField(blank=True, verbose_name='content')),
                ('content_it', models.TextField(blank=True, null=True, verbose_name='content')),
                ('content_en', models.TextField(blank=True, null=True, verbose_name='content')),
                ('excerpt', models.TextField(blank=True, verbose_name='abstract')),
                ('excerpt_it', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('excerpt_en', models.TextField(blank=True, null=True, verbose_name='abstract')),
                ('object_id', models.PositiveIntegerField(blank=True, null=True)),
                ('link', models.URLField(blank=True, null=True, verbose_name='Link')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='videoentry_written', to=settings.AUTH_USER_MODEL, verbose_name='author')),
                ('content_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
                ('cover_img', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='video', to='photologue.Photo', verbose_name='Cover')),
            ],
            options={
                'ordering': ['-publication_start', '-created'],
                'abstract': False,
                'get_latest_by': 'publication_start',
            },
        ),
        migrations.AddField(
            model_name='pressrelease',
            name='videos',
            field=models.ManyToManyField(blank=True, related_name='press_releases', related_query_name='corporate_pressreleases', to='corporate.VideoEntry', verbose_name='related_videos'),
        ),
        migrations.AddField(
            model_name='news',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='news', to='corporate.NewsCategory', verbose_name='Category'),
        ),
        migrations.AddField(
            model_name='news',
            name='content_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='news',
            name='cover_img',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='news', to='photologue.Photo', verbose_name='Cover'),
        ),
        migrations.AddField(
            model_name='news',
            name='gallery',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='news', to='photologue.Gallery', verbose_name='Gallery'),
        ),
        migrations.AddField(
            model_name='news',
            name='tags',
            field=taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
        migrations.AddField(
            model_name='event',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='news', to='corporate.EventCategory', verbose_name='Category'),
        ),
        migrations.AddField(
            model_name='event',
            name='content_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='event',
            name='cover_img',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='event', to='photologue.Photo', verbose_name='Cover'),
        ),
        migrations.AddField(
            model_name='event',
            name='gallery',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='event', to='photologue.Gallery', verbose_name='Gallery'),
        ),
        migrations.AddField(
            model_name='event',
            name='tags',
            field=taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
    ]
