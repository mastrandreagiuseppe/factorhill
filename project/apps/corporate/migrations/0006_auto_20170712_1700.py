# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-07-12 17:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0005_auto_20170711_1523'),
    ]

    operations = [
        migrations.AddField(
            model_name='genericcontentpluginmodel',
            name='full_screen',
            field=models.BooleanField(default=False, verbose_name='Full Screen'),
        ),
        migrations.AddField(
            model_name='genericcontentpluginmodel',
            name='icon',
            field=models.CharField(blank=True, max_length=155, null=True, verbose_name='icon'),
        ),
        migrations.AddField(
            model_name='genericcontentpluginmodel',
            name='link',
            field=models.URLField(blank=True, null=True, verbose_name='Link'),
        ),
    ]
