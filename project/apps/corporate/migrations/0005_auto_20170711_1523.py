# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-07-11 15:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0004_facebookapp_facebookappplugin'),
    ]

    operations = [
        migrations.AddField(
            model_name='facebookapp',
            name='long_term_access_token',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Long Term Access Token'),
        ),
        migrations.AddField(
            model_name='facebookapp',
            name='long_term_access_token_expiring',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Long term access token expiring date'),
        ),
    ]
