# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-07-14 17:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0010_genericcontentpluginmodel_auto_play'),
    ]

    operations = [
        migrations.AddField(
            model_name='genericcontentpluginmodel',
            name='duration',
            field=models.IntegerField(default=3000, verbose_name='Slides speed (in ms)'),
        ),
    ]
