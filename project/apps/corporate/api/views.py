# -*- coding: utf-8 -*-

from photologue.models import Gallery, Photo
from rest_framework import generics

from ..models import Event, EventCategory, News, NewsCategory, VideoEntry, PressRelease
from ..serializers.events import EventSerializer, EventCategorySerializer
from ..serializers.gallery import GallerySerializer, PhotoSerializer
from ..serializers.news import NewsSerializer, NewsCategorySerializer
from ..serializers.pressrelease import PressReleaseSerializer
from ..serializers.videoentries import VideoEntrySerializer


# event api 
class ApiEventDetailView(generics.RetrieveAPIView):
	queryset = Event.published_objects.all()
	serializer_class = EventSerializer

class ApiEventsList(generics.ListAPIView):
    queryset = Event.published_objects.all()
    serializer_class = EventSerializer


class ApiEventCategoryDetail(generics.RetrieveAPIView):
	queryset = EventCategory.published_objects.all()
	serializer_class = EventCategorySerializer

class ApiEventCategoriesList(generics.ListAPIView):
	queryset = EventCategory.published_objects.all()
	serializer_class = EventCategorySerializer



# gallery api views (urls in project.apps.corporate.urls)
class ApiGalleryDetailView(generics.RetrieveAPIView):
	queryset = Gallery.objects.all()
	serializer_class = GallerySerializer

class ApiGalleryListView(generics.ListAPIView):
	queryset = Gallery.objects.all()
	serializer_class = GallerySerializer

class ApiPhotoDetailView(generics.RetrieveAPIView):
	queryset = Photo.objects.all()
	serializer_class = PhotoSerializer

class ApiPhotoListView(generics.ListAPIView):
	queryset = Photo.objects.all()
	serializer_class = PhotoSerializer


#news api views
class ApiNewsDetailView(generics.RetrieveAPIView):
	queryset = News.published_objects.all()
	serializer_class = NewsSerializer

class ApiNewsList(generics.ListAPIView):
    queryset = News.published_objects.all()
    serializer_class = NewsSerializer


class ApiNewsCategoryDetail(generics.RetrieveAPIView):
	queryset = NewsCategory.published_objects.all()
	serializer_class = NewsCategorySerializer

class ApiNewsCategoriesList(generics.ListAPIView):
	queryset = NewsCategory.published_objects.all()
	serializer_class = NewsCategorySerializer

#video entries views (urls in project.apps.corporate.urls)
class ApiVideoEntriesList(generics.ListAPIView):
	queryset = VideoEntry.published_objects.all()
	serializer_class = VideoEntrySerializer


#press release views
class ApiPressReleaseDetail(generics.RetrieveAPIView):
	queryset = PressRelease.published_objects.all()
	serializer_class = PressReleaseSerializer

class ApiPressReleaseList(generics.ListAPIView):
	queryset = PressRelease.published_objects.all()
	serializer_class = PressReleaseSerializer