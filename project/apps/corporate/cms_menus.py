from menus.base import NavigationNode
from cms.menu_bases import CMSAttachMenu
from menus.menu_pool import menu_pool

from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import get_language

from project.apps.corporate.models import NewsCategory, EventCategory


class EventsCategoryMenu(CMSAttachMenu):

    name = _("Events Categories Menu")

    def get_nodes(self, request):
        nodes = []
        categories = EventCategory.published_objects.all()
        i = 0
        for c in categories:
            i = i+1
            nodes.append(NavigationNode(c.name, reverse('events:events-category-list', kwargs={'slug':c.slug}), c.id))
        return nodes

class NewsCategoryMenu(CMSAttachMenu):

    name = _("News Categories Menu")

    def get_nodes(self, request):
        nodes = []
        categories = NewsCategory.published_objects.all()
        i = 0
        for c in categories:
            i = i+1
            nodes.append(NavigationNode(c.name, reverse('news:news-category-list', kwargs={'slug':c.slug}), c.id))
        return nodes


#menu_pool.register_menu(EventsCategoryMenu)
#menu_pool.register_menu(NewsCategoryMenu)
