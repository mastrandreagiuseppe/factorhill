from classytags.core import Options
from classytags.arguments import KeywordArgument, IntegerArgument, StringArgument
from classytags.helpers import InclusionTag
from django import template
from project.apps.corporate.models import MailchimpList

register = template.Library()

class ShowLists(InclusionTag):
    name = 'newsletters_lists_as_hidden'
    template = 'templatetags/dummy.html'


    def get_context(self, context):
        context['items'] = MailchimpList.objects.all()
        return context

register.tag(ShowLists)
