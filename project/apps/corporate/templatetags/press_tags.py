from classytags.core import Options
from classytags.arguments import KeywordArgument, IntegerArgument, StringArgument
from classytags.helpers import InclusionTag
from django import template
from project.apps.corporate.models import PressRelease

register = template.Library()

class ShowPressRelease(InclusionTag):
    name = 'show_last_press'
    template = 'templatetags/dummy.html'

    options = Options(
        IntegerArgument('num', default=1000, required=False),
        IntegerArgument('id', required=False),
    )

    def get_context(self, context, num, id):
        context['num'] = num
        context['id'] = id
        context['items'] = PressRelease.published_objects.exclude(id=id)[:num]
        return context


register.tag(ShowPressRelease)
