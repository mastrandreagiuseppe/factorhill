from classytags.core import Options
from classytags.arguments import KeywordArgument, IntegerArgument, StringArgument
from classytags.helpers import InclusionTag
from django import template
from project.apps.corporate.models import Event

register = template.Library()



class ShowEventsHome(InclusionTag):
    name = 'last_events_home'
    template = 'templatetags/last_events_home.html'

    options = Options(
        IntegerArgument('num', default=1000, required=False),
    )

    def get_context(self, context, num):
        context['num'] = num
        context['items'] = Event.published_objects.all().order_by('-created')[:num]
        return context

register.tag(ShowEventsHome)
