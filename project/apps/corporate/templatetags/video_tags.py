from classytags.core import Options
from classytags.arguments import KeywordArgument, IntegerArgument, StringArgument
from classytags.helpers import InclusionTag
from django import template
from project.apps.corporate.models import VideoEntry

register = template.Library()



class ShowVideos(InclusionTag):
    name = 'last_videos_right'
    template = 'templatetags/dummy.html'

    options = Options(
        IntegerArgument('num', default=1, required=False),
    )

    def get_context(self, context, num):
        context['num'] = num
        context['items'] = VideoEntry.published_objects.all().order_by('-created')[:num]
        return context

register.tag(ShowVideos)
