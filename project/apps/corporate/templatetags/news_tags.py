from classytags.core import Options
from classytags.arguments import KeywordArgument, IntegerArgument, StringArgument
from classytags.helpers import InclusionTag
from django import template
from project.apps.corporate.models import News

register = template.Library()

class ShowNews(InclusionTag):
    name = 'show_last_news'
    template = 'templatetags/dummy.html'

    options = Options(
        IntegerArgument('num', default=1000, required=False),
        IntegerArgument('id', required=False),
    )

    def get_context(self, context, num, id):
        context['num'] = num
        context['id'] = id
        context['items'] = News.published_objects.exclude(id=id)[:num]
        return context

register.tag(ShowNews)
