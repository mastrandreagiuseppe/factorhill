# -*- coding: utf-8 -*-

from django.conf.urls import url

from ..views import NewsDetailView, NewsCategoryListView, NewsCategoryListArchiveView, NewsListView
from ..api.views import ApiNewsDetailView, ApiNewsList, ApiNewsCategoryDetail, ApiNewsCategoriesList

urlpatterns = [
    url(r'^(?P<year>[-\d]+)/(?P<month>[-\d]+)/$', NewsCategoryListArchiveView.as_view(), name='news-list'),
    url(r'^category/(?P<slug>[-\w]+)$', NewsCategoryListView.as_view(), name='news-category-list'),
    

    url(r'^api/news/(?P<pk>\d+)$', ApiNewsDetailView.as_view(), name='api-news-detail'),
    url(r'^api/news/$', ApiNewsList.as_view(), name='api-news-list'),
    
    url(r'^api/news-categories/(?P<pk>\d+)$', ApiNewsCategoryDetail.as_view(), name='api-news-category-detail'),
    url(r'^api/news-categories/$', ApiNewsCategoriesList.as_view(), name='api-news-categories-list'),

    url(r'^(?P<slug>[-\w]+)/$', NewsDetailView.as_view(), name='news-detail'),
    url(r'^$', NewsListView.as_view(), name='news-list'),
]
