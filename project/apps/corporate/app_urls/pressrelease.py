# -*- coding: utf-8 -*-

from django.conf.urls import url

from ..views import PressReleaseDetailView, PressReleaseListView
from ..api.views import ApiPressReleaseDetail, ApiPressReleaseList

urlpatterns = [
    url(r'^api/pressrelease/(?P<pk>\d+)$', ApiPressReleaseDetail.as_view(), name='api-pressrelease-detail'),
    url(r'^api/pressrelease/$', ApiPressReleaseList.as_view(), name='api-pressrelease-list'),

    url(r'^(?P<slug>[-\w]+)/$', PressReleaseDetailView.as_view(), name='pressrelease-detail'),
    url(r'^$', PressReleaseListView.as_view(), name='pressrelease-list'),
]