# -*- coding: utf-8 -*-

from django.conf.urls import url

from ..views import EventDetailView, EventsCategoriesListView, EventCategoryView, EventListView
from ..api.views import ApiEventsList, ApiEventCategoriesList, ApiEventDetailView, ApiEventCategoryDetail

urlpatterns = [
    url(r'^category/(?P<slug>[-\w]+)/$', EventCategoryView.as_view(), name='events-category-list'),
    url(r'^(?P<slug>[-\w]+)/$', EventDetailView.as_view(), name='event-detail'),
    
    url(r'^api/events/(?P<pk>\d+)$', ApiEventDetailView.as_view(), name='api-event-detail'),
    url(r'^api/events/$', ApiEventsList.as_view(), name='api-events-list'),
    
    url(r'^api/event-categories/(?P<pk>\d+)$', ApiEventCategoryDetail.as_view(), name='api-event-category-detail'),
    url(r'^api/event-categories/$', ApiEventCategoriesList.as_view(), name='api-event-categories-list'),
    
    url(r'^$', EventListView.as_view(), name='events-list'),
]