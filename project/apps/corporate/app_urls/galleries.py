# -*- coding: utf-8 -*-

from django.conf.urls import url

from ..views import GalleryList, GalleryDetail

urlpatterns = [
    url(r'^$', GalleryList.as_view(), name='gallery-list'),
    url(r'^(?P<pk>\d+)/$', GalleryDetail.as_view(), name='gallery-detail'),
]
