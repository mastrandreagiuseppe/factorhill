# -*- coding: utf-8 -*-

import datetime
import json
import mailchimp

from project import settings

from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse_lazy, reverse, NoReverseMatch
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, FormView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils.translation import activate, get_language
from django.utils import timezone

from project.apps.base.mixins import AjaxableResponseMixin

from project.settings import MAILCHIMP_API_KEY as key

from .models import NewsCategory, EventCategory, News, Event, SentEmail, VideoEntry, PressRelease, FacebookApp

from .forms import SubscribeToListForm, FacebookAppForm

from menus.utils import set_language_changer
from photologue.models import Gallery, Photo

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       return json.JSONEncoder.default(self, obj)

def getAlternateUrls(object):
    #iterate through all translated languages and get its url for alt lang meta tag                      
    cur_language = get_language()
    altUrls = {}
    for language in settings.LANGUAGES:
        try:
            code = language[0]
            activate(code)
            url = object.get_absolute_url()
            altUrls[code] = url
        finally:
            activate(cur_language)
    return altUrls;


class CorporateDetailView(DetailView):
    url = None

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        set_language_changer(self.request, self.get_absolute_url)
        response = super(CorporateDetailView, self).get(*args, **kwargs)
        return response

    def get_absolute_url(self, language=None):
        return self.object.get_absolute_url()


class NewsDetailView(CorporateDetailView):

    template_name = 'corporate/news_detail.html'
    
    model = News

    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        context['related'] = News.published_objects.exclude(id=self.object.id).exclude(slug__isnull=True).exclude(slug='')
        return context

class EventDetailView(CorporateDetailView):
    url = 'events:event-detail'
    template_name = 'corporate/event_detail.html'

    model = Event

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        context['categories'] = EventCategory.published_objects.all()
        context['related'] = Event.published_objects.exclude(id=self.object.id).exclude(slug__isnull=True).exclude(slug='').filter(event_start_date__gt=timezone.now())
        return context

class PressReleaseListView(ListView):
    url = 'press:pressrelease-list'
    template_name = 'corporate/press_release_list.html'
    paginate_by = 5
    model = PressRelease

    def get_queryset(self):
        if self.request.GET.get('tags'):
            return PressRelease.published_objects.filter(tags__name__icontains=self.request.GET.get('tags')).exclude(slug__isnull=True).exclude(slug='')
        else:
            return PressRelease.published_objects.exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(PressReleaseListView, self).get_context_data(**kwargs)
        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        context['now'] = timezone.now()
        return context

class PressReleaseDetailView(CorporateDetailView):
    url = 'press:pressrelease-detail'
    template_name = 'corporate/press_release.html'

    model = PressRelease

    def get_context_data(self, **kwargs):
        context = super(PressReleaseDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        context['related'] = PressRelease.published_objects.exclude(id=self.object.id).exclude(slug__isnull=True).exclude(slug='')
        return context

class EventsCategoriesListView(ListView):
    template_name = 'corporate/events_categories_list.html'
    url = 'events:events-category-list'
    paginate_by = 50
    #model = EventCategory
    model = EventCategory

    def get_queryset(self):
        return EventCategory.published_objects.exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(EventsCategoriesListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class EventListView(ListView):
    template_name = 'corporate/events_list.html'
    paginate_by = 5
    url = 'events:events-list'
    #model = EventCategory
    model = Event

    def get_queryset(self):
        if self.request.GET.get('tags'):
            return Event.published_objects.filter(tags__name__icontains=self.request.GET.get('tags')).exclude(slug__isnull=True).exclude(slug='')
        else:
            return Event.published_objects.exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(EventListView, self).get_context_data(**kwargs)
        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        if self.kwargs.get('slug', None):
            context['category_slug'] = self.kwargs.get('slug', None)
            context['object'] = EventCategory.published_objects.get(slug=self.kwargs.get('slug'))
        context['now'] = timezone.now()
        return context

class EventCategoryView(CorporateDetailView):
    url = 'events:events-list'
    template_name = 'corporate/events_categories_list.html'
    model = EventCategory

    def get_context_data(self, **kwargs):
        context = super(EventCategoryView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        context['object_list'] = Event.published_objects.filter(category=self.object)
        context['related'] = Event.published_objects.exclude(id=self.object.id).exclude(slug__isnull=True).exclude(slug='').filter(event_start_date__gt=timezone.now())
        return context

class NewsListView(ListView):
    url = 'news:news-list'
    template_name = 'corporate/news_list.html'
    paginate_by = 5
    model = News

    def get_queryset(self):
        if self.request.GET.get('tags'):
            return News.published_objects.filter(tags__name__icontains=self.request.GET.get('tags')).exclude(slug__isnull=True).exclude(slug='')
        else:
            return News.published_objects.all().exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        context['now'] = timezone.now()
        if self.kwargs.get('slug', None):
            context['category_slug'] = self.kwargs.get('slug', None)
            context['object'] = NewsCategory.objects.get(slug=self.kwargs.get('slug'))
        return context

class VideoListView(ListView):
    url = 'videos:video-list'
    template_name = 'corporate/videos.html'
    paginate_by = 20
    model = VideoEntry

    def get_queryset(self):
        if self.request.GET.get('tags'):
            return VideoEntry.published_objects.filter(tags__name__icontains=self.request.GET.get('tags')).exclude(slug__isnull=True).exclude(slug='')
        else:
            return VideoEntry.published_objects.all().exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(VideoListView, self).get_context_data(**kwargs)
        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        context['now'] = timezone.now()
        return context

class GalleryList(ListView):
    url = 'gallery:gallery-list'
    template_name = 'corporate/gallery_list.html'
    paginate_by = 10
    model = Gallery

    def get_context_data(self, **kwargs):
        context = super(GalleryList, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class GalleryDetail(CorporateDetailView):
    url = 'gallery:gallery-detail'
    template_name = 'corporate/gallery_detail.html'

    model = Gallery

    def get_context_data(self, **kwargs):
        context = super(GalleryDetail, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        return context


class NewsCategoryListView(NewsListView):
    url = 'news:news-category-list'
    def get_queryset(self):
        category = self.kwargs.get('slug', None)
        if category:
            return Event.published_objects.filter(category__slug=category).exclude(slug__isnull=True).exclude(slug='')
        else:
            return Event.published_objects.all().exclude(slug__isnull=True).exclude(slug='')

class NewsCategoryListArchiveView(ListView):

    template_name = 'corporate/news_list.html'
    paginate_by = 5
    model = News

    def get_queryset(self):
        created = datetime.datetime.now()
        y = int(self.kwargs.get('year', created.year))
        m = int(self.kwargs.get('month', created.month))

        return News.published_objects.filter(created__year=y, created__month=m).exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(NewsCategoryListArchiveView, self).get_context_data(**kwargs)
        try:
            now = datetime.datetime.now()
            y = int(self.kwargs.get('year', now.year))
            m = int(self.kwargs.get('month', now.month))
            created = datetime.datetime(y, m, 1)
            context['created'] = created

        except Exception:
            pass

        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        context['now'] = timezone.now()
        return context


class MessageCreateAndSend(AjaxableResponseMixin, CreateView):
    model = SentEmail
    fields = ['subject', 'sender_email', 'body']
    success_url = '/test/'

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(MessageCreateAndSend, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            email = EmailMessage(
                self.object.subject,
                "Nuovo messaggio da: " + self.object.sender_email + '\n' + self.object.body,
                'no-reply@factorhill.com',
                ['factorhill.fh@gmail.com',],
                reply_to=[ self.object.sender_email, ],
                #reply_to=['another@example.com'],
                headers={
                    'Message-ID': 'foo',
                },
            )
            
            email.send(False)
            return JsonResponse(data)
        else:
            return response


class SubscribeToListView(FormView):
    form_class = SubscribeToListForm
    success_url = '/thanks/'
    template_name = 'cms_templates/contacts.html'

    def form_invalid(self, form):
        response = super(SubscribeToListView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

        if self.request.is_ajax():
            api = mailchimp.Mailchimp(key)
            list_id = self.request.POST.get('mailchimp_list_id')
            email = self.request.POST.get('email')
            name = self.request.POST.get('name')
            extra = {}
            try:
                if len(name.split()) > 1:
                    extra['FNAME'] = name.split()[0]
                    extra['LNAME'] = ' '.join(name.split()[1:])
                else:
                    extra['FNAME'] = name
                    extra['LNAME'] = ''

                data = api.lists.subscribe(list_id, { 'email': email }, extra)
                l = []
                obj = {}
                obj['message'] = 'OK'
                l.append(obj)
                return JsonResponse(json.dumps(l, cls=SetEncoder), status=200, safe=False)
            except Exception as e:
                l = []
                obj = {}
                obj['error'] = e.message
                l.append(obj)
                return JsonResponse(json.dumps(l, cls=SetEncoder), status=500, safe=False)
        else:
            return response

class GetAccessToken(AjaxableResponseMixin, FormView):
    #model = FacebookApp
    form_class = FacebookAppForm
    success_url = ''
    template_name = 'cms_templates/contacts.html'


    def form_invalid(self, form):
        response = super(GetAccessToken, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(GetAccessToken, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': 'self.object.pk',
            }
            return JsonResponse(data)
        else:
            return response