from modeltranslation.translator import translator, TranslationOptions, register
from .models import Event, EventCategory, News, NewsCategory, VideoEntry, PressRelease


class BaseEntryOptions(TranslationOptions):
    fields = ('title', 'slug', 'subtitle', 'content', 'excerpt' )
    empty_values = {'title': '', 'slug': None}


class BaseCategoryOptions(TranslationOptions):
	fields = ('name', 'slug', 'content', 'excerpt' )
	empty_values = {'name': '', 'slug': None}

@register(NewsCategory)
class NewsCategoryOptions(BaseCategoryOptions):
	pass

@register(EventCategory)
class NewsCategoryOptions(BaseCategoryOptions):
	pass

@register(News)
class NewsOptions(BaseEntryOptions):
	pass

@register(Event)
class EventOptions(BaseEntryOptions):
	pass

@register(VideoEntry)
class VideoEntryOptions(BaseEntryOptions):
	pass

@register(PressRelease)
class PressReleaseOptions(BaseEntryOptions):
	pass

#translator.register(News, NewsTranslationOptions)