# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import PortfolioCategory, PortfolioItem
from .forms import PortfolioItemAdminForm, PortfolioCategoryAdminForm
from modeltranslation.admin import TranslationAdmin
from project.apps.corporate.admin import ModelTranslationTabMixin, BaseTranslationAdmin

from project.apps.base.admin.mixins import OrderableModelAdmin

from tinymce.widgets import TinyMCE


class PortfolioItemAdmin(BaseTranslationAdmin):
    list_display = ('title', 'status', 'subtitle', 'slug' , 'order', )
    form = PortfolioItemAdminForm
    fields = (('title', 'subtitle', 'slug'), ('status', 'publication_start', 'publication_end'), 'content', 'excerpt', ('cover_img', 'gallery'), ('categories'),)
    prepopulated_fields = {"slug": ("title",)}

class PortfolioCategoryAdmin(BaseTranslationAdmin):
    list_display = ('name', 'status', 'slug' , 'order', )
    form = PortfolioCategoryAdminForm
    fields = (('name', 'slug'), ('status', ), 'content', 'excerpt', ('cover_img', ) )
    prepopulated_fields = {"slug": ("name",)}



admin.site.register(PortfolioItem, PortfolioItemAdmin)
admin.site.register(PortfolioCategory, PortfolioCategoryAdmin)
