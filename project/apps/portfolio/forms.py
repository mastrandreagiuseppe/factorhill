# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin
from tinymce.widgets import TinyMCE

from .models import PortfolioItem, PortfolioCategory

class PortfolioItemAdminForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = PortfolioItem
        fields = '__all__'

class PortfolioCategoryAdminForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = PortfolioCategory
        fields = '__all__'