# -*- coding: utf-8 -*-

from project import settings

from django.utils import timezone
from django.views.generic.list import ListView

from photologue.models import Gallery, Photo
from rest_framework import generics


from project.apps.base.mixins import AjaxableResponseMixin

from project.settings import MAILCHIMP_API_KEY as key

from project.apps.corporate.views import getAlternateUrls, CorporateDetailView

from .models import PortfolioCategory, PortfolioItem

from .serializers import PortfolioCategorySerializer, PortfolioItemSerializer


class PortfolioDetailView(CorporateDetailView):
    template_name = 'portfolio/portfolio_detail.html'
    model = PortfolioItem

    def get_context_data(self, **kwargs):
        context = super(PortfolioDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        #context['alt_urls'] = getAlternateUrls(self.object)
        next_item = PortfolioItem.published_objects.filter(order__gt=self.object.order)
        if next_item:
            context['next_item'] = next_item.first()
        
        prev = PortfolioItem.published_objects.filter(order__lt=self.object.order).order_by('-order')
        if prev:
            context['prev'] = prev.first()

        return context

class PortfolioItemsListView(ListView):
    template_name = 'portfolio/portfolio_list.html'
    paginate_by = 10
    #model = PortfolioCategory
    model = PortfolioItem

    def get_queryset(self):
        if self.request.GET.get('tags'):
            return PortfolioItem.published_objects.filter(tags__name__icontains=self.request.GET.get('tags')).exclude(slug__isnull=True).exclude(slug='')
        else:
            return PortfolioItem.published_objects.all().exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(PortfolioItemsListView, self).get_context_data(**kwargs)
        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        context['now'] = timezone.now()
        if self.kwargs.get('slug', None):
            context['category_slug'] = self.kwargs.get('slug', None)
            context['object'] = PortfolioItem.objects.get(slug=self.kwargs.get('slug'))
        return context


class ApiPortfolioCategoryDetailView(generics.RetrieveAPIView):
    queryset = PortfolioCategory.published_objects.all()
    serializer_class = PortfolioCategorySerializer

class ApiPortfolioCategoryList(generics.ListAPIView):
    queryset = PortfolioCategory.published_objects.all()
    serializer_class = PortfolioCategorySerializer


class ApiPortfolioItemCategoryDetail(generics.RetrieveAPIView):
    queryset = PortfolioItem.published_objects.all()
    serializer_class = PortfolioItemSerializer

class ApiPortfolioItemsList(generics.ListAPIView):
    queryset = PortfolioItem.published_objects.all()
    serializer_class = PortfolioItemSerializer

