# -*- coding: utf-8 -*-

from rest_framework import permissions, serializers
from .models import PortfolioItem, PortfolioCategory
from project.apps.corporate.serializers.gallery import GallerySerializer, PhotoSerializer

from project.apps.corporate.serializers import BASE_DETAIL_FIELDS, BASE_CATEGORY_FIELDS, BASE_CATEGORIZED_FIELDS

from sorl.thumbnail import get_thumbnail


class PortfolioCategorySerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    class Meta:
        model = PortfolioCategory
        permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
        fields = BASE_CATEGORY_FIELDS

class PortfolioItemSerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    categories = PortfolioCategorySerializer(many=True, read_only=True)
    gallery = GallerySerializer(read_only=True)
    cover_img = PhotoSerializer(read_only=True)
    absolute_url = serializers.URLField(source='get_absolute_url', read_only=True)
    thumbnail = serializers.SerializerMethodField('retrieve_thumbnail')

    def retrieve_thumbnail (self, obj):
        if(obj.cover_img):
            im = get_thumbnail(obj.cover_img.image, '1920x1280', crop="center", quality=99)
            return im.url
        else:
            return ''
        
    class Meta:
        model = PortfolioItem
        fields = BASE_DETAIL_FIELDS + ( 'absolute_url', 'thumbnail', 'categories')