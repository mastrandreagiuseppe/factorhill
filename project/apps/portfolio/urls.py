# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import PortfolioDetailView, PortfolioItemsListView, ApiPortfolioCategoryDetailView, ApiPortfolioCategoryList, ApiPortfolioItemCategoryDetail, ApiPortfolioItemsList

urlpatterns = [
    url(r'^(?P<slug>[-\w]+)/$', PortfolioDetailView.as_view(), name='portfolio-detail'),
    
    url(r'^api/portfolio-items/(?P<pk>\d+)$', ApiPortfolioCategoryList.as_view(), name='api-portfolio-detail'),
    url(r'^api/portfolio-items/$', ApiPortfolioItemsList.as_view(), name='api-portfolio-list'),
    url(r'^api/portfolio-categories/$', ApiPortfolioCategoryList.as_view(), name='api-portfolio-categories-list'),
    
    url(r'^$', PortfolioItemsListView.as_view(), name='portfolio-list'),
]
