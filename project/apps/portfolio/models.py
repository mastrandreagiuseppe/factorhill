# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.core.urlresolvers import reverse_lazy, reverse

from django.utils.translation import ugettext_lazy as _

from taggit.managers import TaggableManager

from photologue.models import Gallery, Photo

from project.apps.base.models import AdvancedCategory, AdvancedEntryModel
from project.apps.base.mixins import OrderableModelMixin


class PortfolioCategory(AdvancedCategory):
    cover_img = models.ForeignKey(Photo, related_name='portfolio_category', blank=True, null=True, verbose_name=_('Cover'))
    class Meta(AdvancedCategory.Meta):
        verbose_name = _('Portfolio category')
        verbose_name_plural = _('Portfolio categories')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('portfolio:portfolio-category-list', kwargs={'slug':self.slug})


class PortfolioItem(AdvancedEntryModel, OrderableModelMixin):
    gallery = models.ForeignKey(Gallery, related_name='portfolio_items', blank=True, null=True, verbose_name=_("Gallery"))
    exclude_from_menu = models.BooleanField(_('Exclude from main menu'), default=False)
    cover_img = models.ForeignKey(Photo, related_name='portfolio_items', blank=True, null=True, verbose_name=_('Cover'))
    categories = models.ManyToManyField(
        'PortfolioCategory',
        related_name="portfolio_items",
        related_query_name="%(app_label)s_%(class)ss",
        verbose_name=_('related_categories'),
        blank=True
    )

    # This is the important bit - where we add in the tags.
    tags = TaggableManager(blank=True, verbose_name=_("Tags"))

    class Meta(AdvancedEntryModel.Meta):
        verbose_name = _('Portfolio Item')
        verbose_name_plural = _('Portfolio Items')
        ordering = ['order',]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('portfolio:portfolio-detail', kwargs={'slug':self.slug})
