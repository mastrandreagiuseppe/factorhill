from classytags.core import Options
from classytags.arguments import KeywordArgument, IntegerArgument, StringArgument
from classytags.helpers import InclusionTag
from django import template
from ..models import PortfolioItem

register = template.Library()


class ShowPortfolioHome(InclusionTag):
    name = 'last_projects_home'
    template = 'templatetags/last_projects_home.html'

    options = Options(
        IntegerArgument('num', default=1000, required=False),
    )

    def get_context(self, context, num):
        context['num'] = num
        context['items'] = PortfolioItem.published_objects.all().order_by('-created')[:num]
        return context

register.tag(ShowPortfolioHome)
