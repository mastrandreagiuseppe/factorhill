from modeltranslation.translator import translator, TranslationOptions, register
from .models import PortfolioCategory, PortfolioItem


class BaseEntryOptions(TranslationOptions):
    fields = ('title', 'slug', 'subtitle', 'content', 'excerpt' )
    empty_values = {'title': '', 'slug': None}


class BaseCategoryOptions(TranslationOptions):
	fields = ('name', 'slug', 'content', 'excerpt' )
	empty_values = {'name': '', 'slug': None}

@register(PortfolioCategory)
class PortfolioCategoryOptions(BaseCategoryOptions):
	pass

@register(PortfolioItem)
class PortfolioItemOptions(BaseEntryOptions):
	pass

