# -*- coding: utf-8 -*-
from django.db.models import Q
from django.http import Http404
from django.utils.translation import ugettext as _
from django.views.generic.base import TemplateView

from project.apps.corporate.models import News, Event, PressRelease

class GenericSearchView(TemplateView):

    template_name = "corporate/search_results.html"

    def dispatch(self, request, *args, **kwargs):
        # Try to dispatch to the right method; if a method doesn't exist,
        # defer to the error handler. Also defer to the error handler if the
        # request method isn't on the approved list.
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.search =  request.POST.get("search", None)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(GenericSearchView, self).get_context_data(**kwargs)
        
        context["search"] = self.search

        news = News.published_objects.filter(
            Q(title__icontains=self.search) | Q(content__icontains=self.search)
        )
        pressrelease = PressRelease.published_objects.filter(
            Q(title__icontains=self.search) | Q(content__icontains=self.search)
        )
        events = Event.published_objects.filter(
            Q(title__icontains=self.search) | Q(content__icontains=self.search)
        )

        context["news"] = news
        context["events"] = events
        context["pressrelease"] = pressrelease

        context["count"] = news.count() + events.count() + pressrelease.count()

        return context