# -*- coding: utf-8 -*-
# Devo usare FixedSlugField nei models per ovviare a questo problema (risolto in django 1.9)
# https://github.com/deschler/django-modeltranslation/issues/353

import six

from django.db import models
from django import forms


class FixedStripSlugField(forms.SlugField):
    def clean(self, value):
        value = self.to_python(value)
        if isinstance(value, six.string_types):
            value = value.strip()
        return super(forms.SlugField, self).clean(value)

class FixedSlugField(models.SlugField):
    def formfield(self, **kwargs):
        defaults = {'form_class': FixedStripSlugField}
        defaults.update(kwargs)
        return super(FixedSlugField, self).formfield(**defaults)

