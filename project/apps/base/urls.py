# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views.advanced import OrderingView
from .views.base import GenericSearchView

urlpatterns = [
    url(r'^api/save-orderable/$', OrderingView.as_view(), name='save-orderable-view'),
    url(r'^search/$', GenericSearchView.as_view(), name='search-view'),
]
