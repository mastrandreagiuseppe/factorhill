# -*- coding: utf-8 -*-

try:
    # Django >= 1.7
    import json
except ImportError:
    # Django <= 1.6 backwards compatibility
    from django.utils import simplejson as json


class JsonException(Exception):
    json_data = ''
     
    def __init__(self, value):
        self.value = value
        self.json_data = json.dumps({'error': value})
     
    def __str__(self):
        return repr(self.value)
     
     
class RedirectException(Exception):
    """
    per richiamarlo utilizzare 
    
    - send:
        raise RedirectException(reverse_url,**(kw))
    -receive :
        try:    
            self.post_process_view(**(context))
        except RedirectException as e:
            return HttpResponseRedirect(reverse(e.reverse_url, kwargs=e.kw)+ e.querystring)
            
    in kw inserire come chiave
     'chiave_x'               dove chiave_x è <?P(chiave_x)>
     'querystring'            per inserire roba in querystring o
     'is_admin_view' = True   se si vuole richiamare una vista dell'admin
    """
    
    reverse_url = ''
    kw = {}
    querystring = ''
     
    def __init__(self, reverse_url, **kwargs):
        try:
            is_admin_view = kwargs.pop('is_admin_view')
        except KeyError:
            is_admin_view = False 
        try:
            self.querystring = kwargs.pop('querystring')
        except KeyError:
            self.querystring = ''
        self.kw = kwargs.copy()
        self.reverse_url = reverse_url if not is_admin_view else 'admin:'+reverse_url
        
    def __str__(self):
        return repr(self.value)
    

class MaximumFileSizeExceeding(Exception):
    pass

class FileNotSend(Exception):
    pass

class NotImageFile(Exception):
    pass
    