# -*- coding: utf-8 -*-
""" FHCoreLibs > MODELS > Base.py by Frankhood
    @author: Frankhood Business Solutions
"""
import time
import datetime
import itertools

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.sites.models import Site
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db import models
from django.template.defaultfilters import slugify, safe, truncatechars
from django.utils.translation import ugettext_lazy as _

from .managers import (
    CurrentSiteEntryManager, NotPreviewCategoryManager, NotPreviewEntryManager,
    NotPreviewPublishedCategoryManager, NotPreviewPublishedEntryManager,
    PublishedCategoryManager, PublishedEntryManager)
from .mixins import PublicableModelMixin, LocaleModelMixin, OrderableModelMixin, SlugModelMixin

AVAILABLE_CONTENT_TYPES = models.Q(app_label__in=settings.PROJECT_APPS)

class BaseModel(models.Model):
    """ classe model astratta dalla quale tutti i models concreti dovrebbero ereditare """

    created = models.DateTimeField(_("created at"), auto_now_add=True)
    modified = models.DateTimeField(_("modified at"), auto_now=True)
    created_by = models.CharField(_('created by'), max_length=155, blank=True, editable=False)
    modified_by = models.CharField(_('modified by'), max_length=155, blank=True, editable=False)

    class Meta:
        abstract = True
        ordering = ['-created']
        get_latest_by = '-created'

    @property
    def obj_meta(self):
        return self.__class__._meta
    
    @property
    def admin_absolute_url(self):
        from django.core.urlresolvers import reverse
        url_context = {'app_label':self._meta.app_label,'model_name':self._meta.model_name}
        return reverse('admin:{app_label}_{model_name}_change'.format(**url_context),
                       args=(self.id,))

    def app_label(self):
        #Serve per ritrovarsi a template l'app_label
        return self._meta.app_label
    def model_name(self):
        #Serve per ritrovarsi a template il model_name
        return self._meta.model_name
    
class BaseCategory(OrderableModelMixin, SlugModelMixin, BaseModel):
    slugged_field = 'name' 
    
    name = models.CharField(_("name"), max_length=155, blank=True)
    #slug = models.SlugField() #eliminato unique=True x la questione della preview e del linguaggio... 
    #sort_num = models.IntegerField(_("order"), blank=True, default=0)
    css_class = models.CharField(_("CSS class"),  max_length=32, blank=True)

    class Meta(OrderableModelMixin.Meta):
        abstract = True
        #unique_together = ("slug", ) 
        #ordering = ['-created',]
     
    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains",)   

    def __unicode__(self):
        return self.name
    
class BaseLocaleCategory(LocaleModelMixin, BaseCategory):
    
    class Meta(BaseCategory.Meta):
        abstract = True
        unique_together = ("slug", "lang")

class AdvancedCategory(PublicableModelMixin, LocaleModelMixin, BaseCategory):
    
    objects = models.Manager()
    published_objects = PublishedCategoryManager()
    content = models.TextField(_("content"), blank=True)
    excerpt = models.TextField(_("abstract"), blank=True)
    
    class Meta(BaseCategory.Meta):
        abstract = True
     

class BaseEntryModel(SlugModelMixin, BaseModel):
    slugged_field = 'title' 
    
    #on_site = CurrentSiteEntryManager(field_name='sites')
    #sites = models.ManyToManyField(Site, verbose_name=_("published on"))
    
    title = models.CharField(_("title"), max_length=155, blank=True)
    #slug = models.SlugField() #eliminato unique=True x la questione della preview e del linguaggio... 
    subtitle = models.CharField(_("subtitle"), max_length=155, blank=True)
    content = models.TextField(_("content"), blank=True)
    excerpt = models.TextField(_("abstract"), blank=True)
    

    class Meta(BaseModel.Meta):
        abstract = True
        #unique_together = ("slug", ) 
    
    @property
    def abstract(self):
        return safe(self.excerpt) or truncatechars(safe(self.content),200)
    
    def __unicode__(self):
        return self.title
    
    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains",)   
    

class PublicableEntryModel(PublicableModelMixin, BaseEntryModel):
    
    objects = models.Manager() 
    published_objects = PublishedEntryManager()
    
    class Meta(PublicableModelMixin.Meta, BaseEntryModel.Meta):
        abstract = True
        ordering = ['-publication_start', '-created']

class AdvancedLocaleModel(OrderableModelMixin, LocaleModelMixin, PublicableEntryModel):
    
    sites = models.ManyToManyField(Site, verbose_name=_("published on"), blank=True, null=True)
    
    class Meta(OrderableModelMixin.Meta, PublicableEntryModel.Meta):
        abstract = True
        ordering = ['order', '-publication_start', '-created'] 

class AdvancedEntryModel(PublicableEntryModel):
    
    objects = models.Manager() 
    published_objects = PublishedEntryManager()
    
    author = models.ForeignKey(User, verbose_name=_("author"), related_name='%(class)s_written', null=True, blank=True)
    #tags = TagField(_('tags'), blank=True)
    
    # generic relatioship for related objects
    content_type = models.ForeignKey(ContentType,limit_choices_to=AVAILABLE_CONTENT_TYPES,  null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    related_objects = GenericForeignKey()
    
    class Meta(PublicableEntryModel.Meta):
        abstract = True
        ordering = ['-publication_start', '-created']
        #unique_together = ("slug", "is_preview") 

    def __unicode__(self):
        return '%s, %s' %(self.title, self.author)
        
    """
    @property
    def tag_list(self):
        ''' tag list
            @return: list
        '''
        return parse_tag_input(self.tags)
    
    @property
    def has_tags(self):
        return bool(len(self.tag_list))
    
    @property
    def keywords(self):
        ''' keyword list to feed a metatag 
            @return: list
        '''
        kw_data = set(self.title.split() + self.tag_list)
        return list(kw_data)
    
    def get_related(self, related_models=None, limit=3):
        if not related_models:
            related_models = [self.__class__]
        related_list = []
        for model in related_models:
            qs = TaggedItem.objects.get_related(self, model.published_objects.all(), limit)
            related_list.append(qs)
        return list(itertools.chain(*related_list))
    """ 


    
class AdvancedItemModel(LocaleModelMixin, OrderableModelMixin, AdvancedEntryModel):
    
    class Meta(AdvancedEntryModel.Meta):
        abstract = True
        #unique_together = ("slug", "lang")
    

class AdvancedArticleModel(LocaleModelMixin, AdvancedEntryModel):
    
    class Meta(AdvancedEntryModel.Meta):
        abstract = True
        unique_together = ("slug", "lang")
