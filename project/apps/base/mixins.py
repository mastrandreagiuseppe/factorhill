# -*- coding: utf-8 -*-
import datetime
import time
import logging
from django.http import JsonResponse
from django.db import models
from django.db.models import F, Q
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db.models.signals import pre_delete, post_delete
from django.template.defaultfilters import date as django_date, time as django_time
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from . import settings as entry_settings
from .managers import SoftDeleteModelManager, SearchModelMixinManager, PublicableModelManager

from .fields import FixedSlugField

try:
    from modeltranslation.utils import get_language
except ImportError as ex:
    from django.utils.translation import get_language

DRAFT = entry_settings.DRAFT_CODE
PUBLISHED = entry_settings.PUBLISHED_CODE
'''LANG_CHOICES = (("IT",_("italian")), ("FR",_("francaise")),\
                ("EN",_("english")), ("GE",_("german")),\
                ("SP",_("spanish")), ("PO",_("portuguese")))
'''
LANG_CHOICES = settings.LANGUAGES

logger = logging.getLogger(__name__)

class CountModelMixin(models.Model):
    
    counter = models.PositiveIntegerField(_("hit counter"), db_index=True, default=0)
    
    class Meta:
        abstract = True 
        ordering = ['-counter',]
        
    def increment_counter(self, commit=True):
        self.counter += 1
        if commit:
            self.save()

    def decrement_counter(self, commit=True):
        self.counter -= 1
        if commit:
            self.save()


class SlugModelMixin(models.Model):
    slugged_field = 'title' #'title or name or what ever
    force_slugify = False
    
    slug = FixedSlugField(blank=True, null=True) #eliminato unique=True x la questione della preview e del linguaggio... 
    
    class Meta:
        abstract = True 
        #unique_together = ('slug',)
    
    def save(self, *args, **kwargs):
        if self.force_slugify:
            _slugged_field = getattr(self, self.slugged_field)
            try:
                from uuslug import uuslug as slugify
                self.slug = slugify(_slugged_field, instance=self)
            except ImportError:
                from django.utils.text import slugify
                slug = slugify(_slugged_field)[:50]
                #logger.info("******* SLUG : %s"%slug)
                if not self.slug:
                    try:
                        self.__class__._default_manager.get(slug__iexact=slug)
                    except self.__class__.DoesNotExist:
                        self.slug = slug
                    except MultipleObjectsReturned:
                        pass
                    else:
                        #NOTE: non posso basarmi sull'id poichÃ¨ non Ã¨ stato ancora generato.
                        n = datetime.datetime.now()
                        t = int(time.mktime(n.timetuple()))
                        self.slug = '%s-%d' % (slug[:35], t)
                else:
                    if not self.id:
                        try:
                            obj = self.__class__.objects.get(slug=self.slug)
                            if obj != self:
                                n = datetime.datetime.now()
                                t = int(time.mktime(n.timetuple()))
                                self.slug = '%s-%d' % (slug[:35], t)
                        except self.__class__.DoesNotExist:
                            pass
                        except MultipleObjectsReturned:
                            n = datetime.datetime.now()
                            t = int(time.mktime(n.timetuple()))
                            self.slug = '%s-%d' % (slug[:35], t)
        super(SlugModelMixin, self).save(*args, **kwargs) 

class PublicableModelMixin(models.Model):
    
    objects = models.Manager()
    published_objects = PublicableModelManager()
    
    # data
    STATUS_CHOICES = ((DRAFT, _("draft")), (PUBLISHED, _('published')))
    status = models.IntegerField(_("status"), choices=STATUS_CHOICES, default=DRAFT)
    publication_start = models.DateTimeField(_("publication start"), null=True, blank=True)
    publication_end = models.DateTimeField(_("publication end"), null=True, blank=True)
    
    @classmethod
    def get_status_choices(self):
        return self.STATUS_CHOICES
    
    class Meta:
        abstract = True
        ordering = ['-publication_start',]
        get_latest_by = 'publication_start'
        
    @property
    def is_actual(self):
        now = timezone.now()
        #now = datetime.datetime.now()
        return all([ self.publication_start and self.publication_start < now,
                     self.publication_end is None or self.publication_end > now,
                     self.status == PUBLISHED ])
        
    def save(self, *args, **kwargs):
        if self.status is PUBLISHED and not self.publication_start:
            self.publication_start = timezone.now()
        super(PublicableModelMixin, self).save(*args, **kwargs)
    
    def publish(self, commit=True, update_publication_start=False):
        if self.status != PUBLISHED:
            self.status = PUBLISHED
            if update_publication_start and self.publication_start:
                self.publication_start = timezone.now()
            if commit:
                self.save()

    def unpublish(self, commit=True, update_publication_start=False, update_publication_end=False):
        if self.status != DRAFT:
            self.status = DRAFT
            if update_publication_start and self.publication_start:
                self.publication_start = None
            if update_publication_end and self.publication_end:
                self.publication_end = timezone.now()
            if commit:
                self.save()
    
class OrderableModelMixin(models.Model):
    #Settarlo a False nel caso in cui si utilizzi SortableAdmin 
    #django-admin-sortable2
    force_ordering = True
    order = models.PositiveIntegerField(_("position"), db_index=True, default=0, 
                                        blank=True, null=True)
    
    class Meta:
        abstract = True 
        ordering = ['order',]

    def _get_ordering_queryset(self):
        return self.__class__._default_manager.all()
    
    def save(self, *args, **kwargs):
        if self.force_ordering:
            queryset = self._get_ordering_queryset()
            if self.order is None:
                max = queryset.aggregate(models.Max('order'))
                try:
                    self.order = max.get('order__max', 0) + 1
                except TypeError:
                    self.order = 0
            if not self.id:
                qs = queryset.filter(order__gte=self.order).update(order=F('order')+1)
            else:
                try:
                    old_obj = queryset.get(id=self.id)
                    if old_obj.order is not None:
                        if old_obj.order > self.order:
                            qs = queryset.filter(Q(order__gte=self.order),Q(order__lt=old_obj.order)).update(order=F('order')+1)
                        elif old_obj.order < self.order:
                            qs = queryset.filter(Q(order__lte=self.order),Q(order__gt=old_obj.order)).update(order=F('order')-1)
                except (ObjectDoesNotExist, MultipleObjectsReturned) as ex:
                    pass
        return super(OrderableModelMixin, self).save(*args, **kwargs)
    
    def get_previous(self, queryset=None):
        if queryset is None:
            queryset = self._get_ordering_queryset()
        qs = queryset.order_by('-order', '-id').filter(order__lt=self.order)
        if qs.count():
            return qs[0:1].get()
        return None

    def get_next(self, queryset=None):
        if queryset is None:
            queryset = self._get_ordering_queryset()
        qs = queryset.order_by('order', 'id').filter(order__gt=self.order)
        if qs.count():
            return qs[0:1].get()
        return None        


class SoftDeleteModelMixin(models.Model):
    active_objects= SoftDeleteModelManager()
    
    is_removed  = models.BooleanField(_('is removed'), default=False, 
                                      help_text=_('Soft Deleted.'))
    
    class Meta:
        abstract = True 

    def remove(self, commit=True):
        self.is_removed = True
        if commit:
            self.save()
    remove.alters_data=True

    def delete(self, using=None):
        pre_delete.send( sender=self.__class__, instance=self, using=using)
        self.remove()
        post_delete.send( sender=self.__class__, instance=self, using=using)
        logger.info("Post delete send")
    delete.alters_data = True

class LocaleModelMixin(models.Model):
    
    lang = models.CharField(_("language"), choices=LANG_CHOICES, max_length=5)
    
    force_language = True
    
    class Meta:
        abstract = True 
        #REMEMBER to declare lang in the unique_together in the subclass if is_preview
        
    def save(self, *args, **kwargs):
        if self.force_language and not self.lang:
            self.lang = get_language()
        return super(LocaleModelMixin, self).save(*args, **kwargs)



    
class SearchModelMixin(models.Model):
    objects = SearchModelMixinManager()
    
    search_data = models.TextField(_("Campo Aggregatore per ricerca"),
                                   blank=True, null=True)
    
    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "search_data__icontains",)   
    
    
    class Meta:
        abstract=True
        
    def save(self, *args, **kwargs):
        #self.update_status(False)
        if self._can_build_search_data():
            self._build_search_data(**{'commit':False})
        super(SearchModelMixin, self).save(*args, **kwargs)
    
    def _can_build_search_data(self, **kwargs):
        return True if not self.search_data else False
        #return not self.id
    
    def _build_search_data(self, **kwargs):
        commit = kwargs.get('commit',True)
        raise NotImplementedError("Implement build_search_data if you want to search between your data")
        """
        search_array = [x for x in self.ingredients.values_list('name',flat=True)]
        search_array.append(self.player.get_full_name)
        search_array.append(self.title)
        self.search_data = " ".join(search_array)
        """
        if commit:
            self.save()
    _build_search_data.alters_data = True

class PreviewModelMixin(models.Model):
    
    is_preview = models.BooleanField(_("is preview"), default=False)

    class Meta:
        abstract = True 
        #REMEMBER to declare is_preview in the unique_together in the subclass
    
    def save(self, *args, **kwargs):
        # da qui verificare se nei kwargs c'Ã¨ l'indicazione di preview
        return super(PreviewModelMixin, self).save(*args, **kwargs)


class SelfAwareMixin(models.Model):
    """
    Taken from http://www.slideshare.net/erics1/powerful-generic-patterns-with-django
    """
    
    def get_ct(self):
        """
        Returns the Content Type for this instance
        What is returned is cached by Django ---> self.__class__._cache[self.db][key]
        """
        return ContentType.objects.get_for_model(self)

    def get_ct_id(self):
        """ Returns the id of the Content Type for this instance """
        return self.get_ct().pk

    def get_app_label(self):
        """ Returns the app_label of the Content Type for this instance """
        return self.get_ct().app_label

    def get_model_name(self):
        """ Returns the model_name from the Content Type for this instance """
        return self.get_ct().model

    class Meta:
        abstract = True     

class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            return JsonResponse(data)
        else:
            return response