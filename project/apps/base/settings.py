# -*- coding: utf-8 -*-
""" FHCoreLibs > SETTINGS
    @author: Domenico Lamparelli <domenico@frankhood.it> 
    @author: Nicola Mosca <nico@frankhood.it>
"""
from django.conf import settings as s

# Number of days after the entry publication start
# default value is 30
COMMENT_EARLY_CLOSURE = getattr(s, 'ENTRIES_COMMENT_EARLY_CLOSURE', 30)

STAFF_COMMENT_NOTIFICATION =  getattr(s, 'ENTRIES_COMMENT_NOTIFICATION', False)

USE_BITLY = getattr(s, 'ENTRIES_USE_BITLY', 'django_bitly' in s.INSTALLED_APPS)

USE_TINYMCE = getattr(s, 'ENTRIES_USE_TINYMCE', 'tinymce' in s.INSTALLED_APPS)

ENABLE_RATINGS = getattr(s, 'ENTRIES_ENABLE_RATINGS', 'djangoratings' in s.INSTALLED_APPS)

DEFAULT_ENTRIES_FOR_WIDGET = getattr(s, 'ENTRIES_FOR_WIDGET', 3)

COLORPICKER_COLORS = getattr(s, 'COLORPICKER_COLORS', ['b4da35','37af68','64cf00','cfcc00','fdb735'])

# Those two values should never change
# I've stored them here to resolve some circular import issues
DRAFT_CODE = 0
PUBLISHED_CODE = 99
