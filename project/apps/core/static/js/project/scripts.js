(function ($, window, document, undefined) {
    'use strict';

    $(window).load(function(){
    	
    	$("#loading_screen img").addClass("fadeOutUp").delay(300).queue(function(next){
    		next();
    	});

		$("#loading_screen .text").addClass("fadeOutDown").delay(300).queue(function(next){
    		next();
    	});

		$("#loading_screen").addClass("invis").delay(300).queue(function(next){
    		next();
	    	$("#wrapper").addClass("visible").delay(300).queue(function(next){
	    		next();
	    	});
    	});

		initNavbar();
		initFooter();
    	initPortfolio();
    	initScrollToTop();
    	initBlogGrid();
    	setTimeout(function(){
    		initSliders();
    	}, 500);

    	$('.equal_heights').equalHeights();
    });

    function validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

    function initBlogGrid(){

    	//blog grid
	    if ($('.blog_posts.grid').length){
		  	var $container = $(".blog_posts.grid");

			$container.isotope({
				itemSelector: '.post',
				resizable: false,
				filter: '*',
				layoutMode: 'packery'
			});
			setTimeout(function(){
				$container.isotope('layout');
			}, 100);

		    $(window).on("debouncedresize", function(event){
				$container.isotope('layout');
		    });
		}

    }

    function initNavbar(){
    	$(".main_menu li a")
		.on( "mouseenter", function() {
			var page = $(this).attr("data-page");

			var submenu = $('#submenu_bar');
			var submenu_page = $('#submenu_bar .menu[data-page="'+page+'"]');

			if (submenu_page.length){
				submenu.addClass("visible");
				$('#submenu_bar .menu').removeClass("visible");
				submenu_page.addClass("visible");
			}else{
				$("#submenu_bar").removeClass("visible");
				$('#submenu_bar .menu').removeClass("visible");
			}

		})
		.on( "mouseleave", function(e) {
		});

    	$("#submenu_bar")
		.on( "mouseenter", function() {

		})
		.on( "mouseleave", function(e) {
			$("#submenu_bar").removeClass("visible");
			$('#submenu_bar .menu').removeClass("visible");
		});
    }

    function initFooter(){
	    var footerHeight = $("#footer")[0].scrollHeight;
	    $("body").css("padding-bottom", footerHeight);
    }

    function initPortfolio(){

    	$(".portfolio_grid_filter_toggle").on("click", function(){

    		var filter_hidden = $(".portfolio_grid_filter:not(.visible)").hasClass("hidden");

    		if (filter_hidden){
    			$(this).addClass("active");
    			$("#content").addClass("floating_filter");
    			setTimeout(function(){
    				$(".portfolio_grid_filter").addClass("visible");
    			}, 250);
    		}else{
    			$(this).removeClass("active");
    			$(".portfolio_grid_filter").removeClass("visible");
    			setTimeout(function(){
    				$("#content").removeClass("floating_filter"); 
    			}, 250);
    		}

    		return false;

    	});

    	//portfolio grid
	    if ($('.masonryWrapper').length){
		  	var portfolioCols = $(".masonryWrapper").attr("data-cols");
		    var $workItems = $('.masonryWrapper'),
		    colWidth = function () {
		      console.log('colWidth');
		      var w = $workItems.width(), 
		      columnNum = 1,
		      columnWidth = 0;
		      if (w > 1170) {
		        columnNum  = ((portfolioCols == undefined) ? 3 : portfolioCols);
		      } 
		      else if (w > 960) {
		        columnNum  = 3;
		      } 
		      else if (w > 640) {
		        columnNum  = 3;
		      } 
		      else if (w > 480) {
		        columnNum  = 2;
		      }  
		      else if (w > 360) {
		        columnNum  = 1;
		      } 
		      columnWidth = Math.floor(w/columnNum);
		      $workItems.find('.item').each(function() {
		        var $item = $(this),
		        multiplier_w = $item.attr('class').match(/item-w(\d)/),
		        multiplier_h = $item.attr('class').match(/item-h(\d)/),
		        width = multiplier_w ? columnWidth*multiplier_w[1] : columnWidth,
		        height = multiplier_h ? columnWidth*multiplier_h[1]*0.75-9 : columnWidth*0.76-10;
		        
		        $item.css({
		          width: width - parseInt($item.css('margin'))*2,
		          //height: height
		        });
		      });
		      return columnWidth;
		    },
		    isotope = function () {
		    	console.log('isotope');
		      	$workItems.isotope({
			        resizable: true,
			        itemSelector: '.item',
			        masonry: {
			          	columnWidth: colWidth(),
			          	gutterWidth: 10
			        }
		      	});

		    };
		    $workItems.imagesLoaded().progress( function() {
		    	console.log('imagesLoaded');
			  	isotope();
			});
		    $(window).on("debouncedresize", function(event){
		    	console.log('debouncedresize');
		    	isotope();
		    });
		}

	    //portfolio filter
	    $('.portfolio_grid_filter li').on('click', function(){

	    	if ($(this).hasClass("label"))
	    		return false;

		    $('.portfolio_grid_filter li').removeClass('active');
		    $(this).addClass('active');

	    	var selector = $(this).attr('data-filter');
			$workItems.isotope({
				filter: selector
			});
			return false;
	    });
    }

    function initScrollToTop(){
    	var scrollTop = $(window).scrollTop();

    	var scrollToTop = $(".scroll_to_top");

    	if (scrollTop > 200){
    		scrollToTop.addClass("visible");
    	}else{
    		scrollToTop.removeClass("visible");
    	}

    }

    function initSliders(){
    	//clients slider
    	$(".clients_list").owlCarousel({
    		items:4,
    		nav: false,
    		dots: true,
    		loop:true,
		    autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:4
		        }
		    }
    	});

    	//testimonials slider
    	$(".testimonials_items").owlCarousel({
    		items:1,
    		nav: false,
    		dots: true
    	});

    	//slideshow
    	$(".slideshow").each(function(){
    		var has_dots = false;
    		if ($(this).hasClass("with_dots")){
    			has_dots = true;
    		}

    		var has_nav = false;
    		if ($(this).hasClass("with_nav")){
    			has_nav = true;
    		}

    		var autoplay = false;
    		var autoplay_timeout = 3000;
    		if ($(this).hasClass("autoplay")){
    			autoplay = true;
    			if ($(this).attr("data-autoplay-timeout") != ""){
    				autoplay_timeout = $(this).attr("data-autoplay-timeout");
    			}
    		}

    		var params = {
	    		items:1,
	    		loop: true,
	    		autoplaySpeed: 1000,
	    		nav: has_nav,
	    		navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
	    		dots: has_dots
	    	};

	    	if (autoplay == true){
	    		params["autoplay"] = autoplay;
	    		params["autoplay_timeout"] = autoplay_timeout;
	    	}

	    	params['autoplayHoverPause'] = false;

	    	$(this).owlCarousel(params);

	    	if ($(this).hasClass("fullscreen")){
	    		var parentHeight = $(window).height();
	    		$(".slideshow .slide").css("height", parentHeight);
	    	}
	    });
    }

    if ($(".fullscreen_video").length){
    	$(".fullscreen_video").each(function(){

    		var video_id = $(this).attr("data-video-id");
    		$(this).YTPlayer({
				videoId: video_id
			});

    	});
    }

    if ($(".fullscreen_personal_bg").length){
    	var winHeight = $(window).height();
    	$(".fullscreen_personal_bg").css("height", winHeight);
    }

    $(".scroll_to_top").on("click", function(e){
    	e.preventDefault();

		e.preventDefault();
		$("html, body").animate({scrollTop:0}, 800);

    });

    $(window).scroll(function(){
    	initScrollToTop();
    });
    
	if ($("a[data-lightbox^='image_lightbox']").length){
		$("a[data-lightbox^='image_lightbox']").magnificPopup({type:'image'});
	}

	$("a[data-lightbox^='image_lightbox']").on('click', function(e){
		e.preventDefault();
		e.stopPropagation();
		return false;
	});

	if ($("#price_filter").length){
	    $("#price_filter").slider({
	        range: true,
	        min: 0,
	        max: 2500,
	        values: [200, 1300],
	        slide: function (event, ui) {
	            $(".price_filter_min_val span").html(ui.values[0]);
	            $(".price_filter_max_val span").html(ui.values[1]);
	        }
	    });
	    $(".price_filter_min_val span").html($("#price_filter").slider("values", 0));
	    $(".price_filter_max_val span").html($("#price_filter").slider("values", 1));
	}

	$(".product_quantity_lower").on("click", function(){

		var input = $(".product_quantity_input").val();
		input--;

		if (input < 0)
			input = 0;

		$(".product_quantity_input").val(input);

		return false;

	});

	$(".product_quantity_increase").on("click", function(){

		var input = $(".product_quantity_input").val();
		input++;

		$(".product_quantity_input").val(input);

		return false;

	});

    //contact form
    $("body").on("click", ".sentMessage", function(e){
    	e.preventDefault();
    });
    $("body").on("click", ".sendingEmail", function(e){
    	e.preventDefault();
    });

	$(".#contactForm").on("submit", function(e){
		e.preventDefault();
		var btn = $(e.target);
		var name = $(this).parents(".contact_form").find(".cf_name");
		var email = $(this).parents(".contact_form").find(".cf_email");
		var message = $(this).parents(".contact_form").find(".cf_message");
		var val = $(this).val();

		debugger;

		btn.removeClass('cf_send_message').addClass('sendingEmail');


		name.removeClass('contains_error');
		if(!name.val()){
			name.addClass('contains_error');
		}

		email.removeClass('contains_error');
		if(!email.val() || !validateEmail(email.val())){
			email.addClass('contains_error');
		}

		message.removeClass('contains_error');
		if(!message.val()){
			message.addClass('contains_error');
		}

		if(!!$('.contains_error').length){
			btn.removeClass('sendingEmail').addClass('cf_send_message');
			return false;
		}else{
			$('.loaderWrapper').addClass('active');
			$(this).val("Loading...");

			$.ajax({
				type: 'POST',
				headers: {
					'X-CSRFToken': $('.contact_form').find('[name=csrfmiddlewaretoken]').val()
				},
				url: Vars.urls.postMessageUrl,
				data: {
					'subject' : name.val(),
					'sender_email' : email.val(),
					'body' : message.val()
				},
				dataType: 'json',
				error: $.proxy(function(data, status, obj) {
					btn.removeClass('sendingEmail');
					$('.loaderWrapper').removeClass('active');
						btn.removeClass('cf_send_message');
						/*
						if (data.error == false){
							name.val('');
							email.val('');
							message.val('');
							$(".contact_form .inputfield").removeClass("contains_error");
						}else{
							$(".contact_form .inputfield").removeClass("contains_error");
							for (var i = 0; i < data.error_fields.length; i++) {
								$(".contact_form .cf_"+data.error_fields[i]).addClass("contains_error");
							};
						}
						*/

					$(".contact_form .messages").html('Errore nella spedizione della mail. Riprova più tardi!').addClass("visible");
					$(this).val(val);

				}, this),
				success: $.proxy(function(data, status, obj) {
					btn.removeClass('sendingEmail');
					$('.loaderWrapper').removeClass('active');
					if(status === 'success'){
						btn.html('MESSAGGIO INVIATO').addClass('sentMessage disabled');
					}else{
						btn.removeClass('cf_send_message');
						/*
						if (data.error == false){
							name.val('');
							email.val('');
							message.val('');
							$(".contact_form .inputfield").removeClass("contains_error");
						}else{
							$(".contact_form .inputfield").removeClass("contains_error");
							for (var i = 0; i < data.error_fields.length; i++) {
								$(".contact_form .cf_"+data.error_fields[i]).addClass("contains_error");
							};
						}
						*/

						$(".contact_form .messages").html('Errore nella spedizione della mail. Riprova più tardi!').addClass("visible");

						$(this).val(val);
					}

				}, this)
			});

			return false;
		}

	});


	//google maps
	if ($("#gmap").length){

		var marker;
	    $("#gmap").appear();
	    $("body").on("appear", "#gmap", function(event, $all_appeared_elements){

	    	if (!$(this).hasClass("done")){

				//Google map
				var theme_array = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"administrative.country","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#062340"}]},{"featureType":"administrative.country","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"labels","stylers":[{"visibility":"simplified"},{"color":"#d8d8d8"}]},{"featureType":"administrative.province","elementType":"labels.text.fill","stylers":[{"color":"#f0f0f0"}]},{"featureType":"administrative.province","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#132e4a"}]},{"featureType":"administrative.locality","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.neighborhood","elementType":"labels","stylers":[{"visibility":"on"},{"color":"#ff0000"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#132e4a"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#dedede"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#d9e2d9"},{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#e2e0e0"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#f1f1f1"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#c9e0e9"},{"visibility":"on"}]}];

			    var map = new google.maps.Map(document.getElementById('gmap'), {
				    zoom: 18,
					center: {lat: 41.053320, lng: 16.705076},//latitude and longitude
			        styles: theme_array,
					scrollwheel: false
				});

				var contentString = '<h3>Rigenera</h3><h5>Where you can be yourself</h5><p>Via della Resistenza, snc <br /> 70027 - Palo del Colle (BA) </p>';

				var infowindow = new google.maps.InfoWindow({
					content: contentString
				});

				marker = new google.maps.Marker({
					map: map,
					draggable: true,
					animation: google.maps.Animation.DROP,
					position: {lat: 41.053320, lng: 16.705076},//latitude and longitude
					title: 'Rigenera - Where you can be yourself'
				});
				marker.addListener('click', function(){
					infowindow.open(map, marker);

					toggleBounce();
				});

				$(this).addClass("done");
		    }

	    });
	}

	//gallery
	$('.popup-gallery').magnificPopup({
		delegate: 'a.icon',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
		}
	});

	$(window).resize(function(){

		if ($(window).width() > 767){
			$("body").removeClass("in_mobile");
		}else{
			$("body").addClass("in_mobile");
		}

	});

	$("body").on("click", ".mobile_nav_toggle", function(){
		
		var menu = $("#submenu_bar");

		if (menu.hasClass("mobile_visible")){
			menu.removeClass("mobile_visible");
		}else{
			menu.addClass("mobile_visible");
		}

		return false;
	});

	//check for mobile
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	//if not mobile
	if( !isMobile.any() ){

		$(".parallax_item").each(function(){
			$(this).parallax("50%", 0.4);
		});

		$(window).load(function(){

			//init WOW animations
			new WOW().init();

			if ($(window).width() <= 767){
				$("body").addClass("in_mobile");
			}

		});

	    //counter up
	    $(".counter_up").appear();
	    $("body").on("appear", ".counter_up", function(event, $all_appeared_elements){

	    	if (!$(this).hasClass("animated")){
				$(this).counterUp({
					delay: 10,
					time: 1000
				});
				$(this).addClass("animated");
		    }

	    });

	    //progress bars
	    $(".progress_bars .item").appear();
	    $("body").on("appear", ".progress_bars .item", function(event, $all_appeared_elements){

	    	if (!$(this).hasClass("animated")){
	    		var percentage = $(this).find(".bar_inner").attr("data-percent");
	    		$(this).find(".bar_inner").animate({
	    			width: percentage
	    		}, 1000);

				$(this).addClass("animated");
		    }

	    });

	}else{
		//when on mobile device

		//dont do parallax effect, instead show static image
		$(".parallax_item").addClass("no_parallax");

		$("body").addClass("in_mobile");


	}

	$('.js-share').on('click', function(e){
		e.preventDefault();
		var target = $(e.target),
			absoluteUrl = target.parents('.social_icons').data('absolute_url'),
			social = target.data('social'),
			w, desc;

		function generateUrl(name, desc){
            var s;
            switch(name){
            	case 'fb':
            		s = "https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent(absoluteUrl);
            		break;
	            case 'twitter':
	                s = "http://twitter.com/intent/tweet/?text="+ encodeURIComponent($("h1.title").text().trim()) +"&url="+ encodeURIComponent(absoluteUrl)/*+"&via=rigenera"*/;
	                break;
	            case 'linkedin':
	                s = "https://www.linkedin.com/shareArticle?url="+encodeURIComponent(absoluteUrl)+"&mini=true&title="+ encodeURIComponent($("h1.title").text().trim()) +"&source="+encodeURIComponent('www.oneworks.com')+"&summary="+encodeURIComponent(desc);
	                break;
	            case 'tumblr':
	                s = "http://www.tumblr.com/share?v=3&u="+ encodeURIComponent(absoluteUrl); //+"&t=";
	                break;
	            case 'gplus':
	                s = "https://plus.google.com/share?url="+ encodeURIComponent(absoluteUrl); //+"&t=";
	                break;
	        }
            return s;
        }
		
        function createPopup(name, desc){
            var width  = 625,
                height = 500,
                left   = ($(window).width()  - width)  / 2,
                top    = ($(window).height() - height) / 2,
                url    = generateUrl(name, desc),
                opts   = 'status=1' +
                         ',width='  + width  +
                         ',height=' + height +
                         ',top='    + top    +
                         ',left='   + left;
            window.open(url, name, opts);
            return false;
        }

        desc = $(".post section.body").text().trim().substring(0, 64).split(" ").slice(0, -1).join(" ") + "...";

        createPopup(social, desc);
	});
	console.log('test');
	window.shareBtnsHandler = new ShareBtns({
	    btns: [
	      '.js-fbShare', '.js-twitterShare', '.js-mailShare'
	    ],
	    title: $('meta[property="og:title"]').prop('content'),
	    url: $('meta[property="og:url"]').prop('content'),
	    description: $('meta[property="og:description"]').prop('content')
	});


	$('.js-customTeam').on('click', function(e){
		console.log(e.target);
	})

})(jQuery, window, document);