;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');

    Models.Generic = Models.BaseModel.extend({
        //idAttribute: 'slug',
        urlRoot:function (options){
            return Urls.usersListUrl || 'testUrl';
        }
    });

    Collections.Generic = Collections.BaseCollection.extend({
        models : Models.Generic,
        contextObjectName : 'results',
        url : function (options){
            return Urls.usersListUrl || 'testUrlCollections';
        }
    });

})();