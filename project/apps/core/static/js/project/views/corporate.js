;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    
    Views.CorporateDetailView = Views.DetailView.extend({
        

        tagName: 'div',

        attributes: function(){
            var me = this;
            return {
                href: me.model.get('absolute_url')
            };
        },

        className: function(){
            var me = this,
                s = 'blog-i';
            
            return s;
        }
        
    });

    Views.CorporatePageView = Views.ListView.extend({
        preInitialize : function(options){
            var me = this;
            console.log("PortfolioPageView preInitialize");
            
            me._super(Views.ListView, 'preInitialize', arguments);

            //me.baseChildrenFilterView.render();
        },

        postRender: function(){
            var me = this;
            
            me._super(Views.ListView, 'postRender', arguments);

            me.customPostRender();
        },

        bindEvents: function(){
            var me = this;
            me._super(Views.ListView, 'bindEvents', arguments);
            me.listenTo(me.channel, 'filter', me.onFilter);
        },

        renderChildrenViews : function() {
            var me = this;
            var $wrapper;
            if(me.baseChildrenWrapper){
                $wrapper =me.$(me.baseChildrenWrapper);
            }
            else{
                $wrapper = me.$el;
            }
            //var $wrapper = me.$(me.baseChildrenWrapper);//(me.baseChildrenWrapper && me.$(me.baseChildrenWrapper)) || me.$el;
            $wrapper.empty();
            if(me.childrenCollection.length===0){
                $wrapper.append(me.emptyCollection);
            }else{
                var el = '';
                _.each(me.childrenCollection, function(view, index) {
                    if(index%7 === 0){
                        el += '<div class="pageWrapper">';
                    }
                    view.render();
                    el += $(view.el)[0].outerHTML;
                    if(index%7 === 6){
                        el += '</div>';
                    }
                });
                
                $wrapper.append(el);
            }
            return me;
        }
    });

})();