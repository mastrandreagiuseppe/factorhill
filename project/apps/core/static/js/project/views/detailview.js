;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    
    Views.DetailView = Views.BaseDetailView.extend({
        
        modelContextName: 'model',
        autoSave: false,

        hasCoverImage: true,

        preloadImagePlaceholder: function(){
            return "<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 {{w}} {{h}}'><defs><symbol id='a' viewBox='0 0 90 66' opacity='0.3'><path d='M85 5v56H5V5h80m5-5H0v66h90V0z'/><circle cx='18' cy='20' r='6'/><path d='M56 14L37 39l-8-6-17 23h67z'/></symbol></defs><use xlink:href='#a' width='20%' x='40%'/></svg>";
        },

        events : {
        },

        tagName: 'a',

        attributes: function(){
            var me = this;
            return {
                href: me.model.get('absolute_url')
            };
        },

        preInitialize : function(options){
            var me = this;
            console.log("DetailView preInitialize");

            me.channel = ChannelBroker.get("AppChannel");

            _.each(options, function(v, k){
                me[k] = v;
            });

            me.templateId = Vars.detailTemplateId;

            if(!!me.modelName){
                me.model = new me.modelName();
            }
        },

        getContext: function(){
            var me = this,
                context = {};

            context[me.modelContextName] = me.model.toJSON();
            context['preloadImage'] = "data:image/svg+xml;utf-8," + me.preloadImagePlaceholder();
            context['globals'] = Vars;
            return context;
        },

        postRender: function(){
            var me = this;
            console.log('DetailView postRender');

            me.preloadImage();
        },

        preloadImage: function(){
            var me = this;
            if(me.hasCoverImage && _.isObject(me.model.get('cover_img'))){
                var img = new Image();
                img.onload = function(){
                    $('img', me.$el).attr('src', img.src);
                    $('img', me.$el).removeClass('imageReplacer');
                };

                img.src = me.model.get('cover_img').image;
            }
        },

        slugify: function(s){
            return s
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'')
                ;
        },

        removeEvents: function() {
            this.model.off('all');
        }
    });

})();