;
(function() {
    var Models = namespace('Project.Models'),
        Collections = namespace('Project.Collections'),
        Routers = namespace('Project.Routers'),
        Urls = namespace('Project.Urls'),
        Vars = namespace('Project.Vars'),
        Objects = namespace('Project.Objects'),
        Views = namespace('Project.Views');


    Views.CycleGalleryView = Views.BaseView.extend({
        
        slideSelector: '.sl-slide',
        nextSelector: '.nav-arrow-next',
        prevSelector: '.nav-arrow-prev',
        navDotsSelector: '#nav-dots',
        pagerActiveClass: 'nav-dot-current',
        pagerTemplate: '<span></span>',

        preRender: function(){
            var me = this;
            me.slides = [];

            _.each($(me.slideSelector, me.$el), function(v, i){
                var view;

                if($(v).hasClass("videoSlide")){
                    view = new Views.VideoDetailView({
                        $el: me.$el,
                        model: new Models.BaseModel({
                            link: $(v).data('link'),
                            id: $(v).data('id'),
                            cover_img: {
                                image: window.location.protocol + '//' + window.location.host + '/media/' + $(v).data('cover_img')
                            }
                        }),
                        bindEvents: function(){
                            var me = this;
                            $('.js-playVideo').on('click', function(e){
                                e.preventDefault();
                                me.onJsPlayVideo();
                            });
                        },
                        afterThumbLoad: function(img){
                            var me = this;
                            TweenMax.staggerTo($('.videoPlayerWrapper .video-js', me.$el), 0.3, {autoAlpha: 0});
                            $('.videoPlayerWrapper', me.$el).css('background-image', 'url('+img.src+')');
                            $('.videoPlayerWrapper', me.$el).css('background-size', 'cover');
                            $('.videoPlayerWrapper', me.$el).css('background-position', 'center center');
                        },
                        onJsPlayVideo: function(){
                            var me = this;
                            var playpause = $(".playVideoSvgWrapper svg")[0],
                                lefttoplay = $("#lefttoplay")[0],
                                righttoplay = $("#righttoplay")[0],
                                lefttopause = $("#lefttopause")[0],
                                righttopause = $("#righttopause")[0];
                            
                            playpause.style.display = "block";

                            if(this.videoPlayer.paused()){
                                this.videoPlayer.play();
                                TweenMax.staggerTo(
                                    '.js-hideWhilePlaying',
                                    0.2, 
                                    { autoAlpha: 0 },
                                    0.2,
                                    function(){
                                        playpause.classList.add("playing");
                                        lefttopause.beginElement();
                                        righttopause.beginElement();
                                        TweenMax.staggerTo($('.videoPlayerWrapper .video-js', me.$el), 0.3, {autoAlpha: 1});
                                    }
                                );
                            }else{
                                TweenMax.staggerTo(
                                    $('.videoPlayerWrapper .video-js', me.$el),
                                    0.2, 
                                    { autoAlpha: 0 },
                                    0.2,
                                    function(){
                                        me.videoPlayer.pause();
                                        lefttoplay.beginElement();
                                        righttoplay.beginElement();
                                        playpause.classList.remove("playing");
                                        TweenMax.staggerTo('.js-hideWhilePlaying', 0.3, {autoAlpha: 1});
                                    }
                                );
                            }
                        }
                    });
                    
                    me.slides.push(view);
                    $('video', view.$el).height($(window).height());
                    
                    view.videoPlayer = videojs($('video', me.$el)[0], {
                        controls: false,
                        autoplay: view.$el.hasClass('js-autoPlay'),
                        width: $(window).width(),
                        height: $(window).height(),
                        techOrder: ["youtube", "html5"],
                        sources: [
                            { "type": "video/youtube", "src": view.model.get('link') }
                        ],
                        youtube: {
                            "ytControls": 0
                        },
                        nativeControlsForTouch: true
                    });
                    

                    $('.videoSlide .autoPlay').css('opacity', 1);
                    view.preloadImage();

                }else{
                    view = new Views.VideoDetailView({
                        model: new Models.BaseModel({
                            id: $(v).data('id'),
                            cover_img: {
                                image: window.location.protocol + '//' + window.location.host + '/media/' + $(v).data('cover_img')
                            }
                        })
                    });
                    view.$el = $(v);
                    me.slides.push(view);
                }
            });
        },

        postRender: function(){
            var me = this,
                $slider = me.$el,
                w = $(window).height();

            if(!!$('.genericNs').length){
                if($slider.hasClass('fullScreen')){
                    if($(window).outerWidth() < 768){
                        $('#page').height($(window).width());
                        $('.topSlider').height($(window).width());    
                        $slider.height($(window).width());
                    }else{
                        $('#page').height(w);
                        $('.topSlider').height(w);    
                        $slider.height(w);
                    }
                }else{
                    console.log('gallery non fullscreen');
                }
                
            }else{
                if($slider.hasClass('fullScreen')){
                    $('#page').height(w);
                    $('.topSlider').height(w);    
                    $slider.height(w);
                }else{
                    console.log('gallery non fullscreen');     
                }
            }

            $('.sliderWrapper', me.$el).on( 'cycle-before', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag){
                console.log(me);
                if($(outgoingSlideEl).hasClass('videoSlide')){
                    _.each(me.slides, function(v, i){
                        if(v.model.get('id') === $(outgoingSlideEl).data('id')){
                            v.videoPlayer.pause();
                        }
                    });
                }
                if($(incomingSlideEl).hasClass('videoSlide')){
                    _.each(me.slides, function(v, i){
                        if(v.model.get('id') === $(incomingSlideEl).data('id')){
                            //v.videoPlayer.play();
                        }
                    });
                }
            });
            $('.sliderWrapper', me.$el).cycle({
                slides           : me.slideSelector,
                next             : me.nextSelector,
                paused           : (me.$el.data('autoplay') === 'False') ? true : false,
                prev             : me.prevSelector,
                pager            : me.navDotsSelector,
                pagerActiveClass : me.pagerActiveClass,
                pagerTemplate    : me.pagerTemplate,
                timeout          : !!me.$el.data('duration') ? me.$el.data('duration') : 8000
            });
        }
    });

    Views.PageView = Views.BaseView.extend({

        el: 'body',

        pageResize: function() {
            // #page top & bottom paddings 30*2=60
            var page_padding = 60;
            // .page-cont-wrap top & bottom paddings 70*2=140
            var cont_padding = 140;
            if ($(window).width() <= 533) { // 550
                page_padding = 30;
            }
            var window_ht = $(window).height();
            var header_ht = $('.header').outerHeight();
            var lh = window_ht - header_ht - page_padding - cont_padding;
            $('.page-cont-wrap').css('line-height', lh + 'px');
        },

        equalize_height: function(elements, cols, el_class) {
            var max_height = 0;
            $(elements).outerHeight('auto');
            $(elements).each(function(i) {
                var $el_index = $(this).index();
                var $col_elements = $(this);
                max_height = $(this).outerHeight();
                if (i % cols === 0) {
                    for (var j = 1; j < cols; j++) {
                        if ($(elements).eq($el_index + j).hasClass(el_class)) {
                            max_height = Math.max(max_height, $(elements).eq($el_index + j).outerHeight());
                            $col_elements = $col_elements.add($(elements).eq($el_index + j));
                        }
                    }
                    $col_elements.outerHeight(max_height);
                }
            });
            return false;
        },

        postInitialize: function() {
            this.render();
        },


        postRender: function() {
            console.log('postRender');
            var me = this,
                useFacebook = !!window.useFacebook;
            
            window.shareBtnsHandler = new ShareBtns({
                btns: [
                  '.js-fbShare', '.js-twitterShare', '.js-mailShare'
                ],
                title: $('meta[property="og:title"]').prop('content'),
                url: $('meta[property="og:url"]').prop('content'),
                description: $('meta[property="og:description"]').prop('content')
            });

            if(useFacebook) {
                me.feedView = new Views.FacebookFeedView({
                    appId: window.appId,
                    accessToken: window.accessToken
                });
            }

            if ($('#page').length > 0) {
                $(window).resize(function() {
                    me.pageResize();
                });
                me.pageResize();
            }

            if($('.js-crossfade').length){
                var images = document.querySelectorAll('.js-crossfade .logo'),
                    c = 0;

                images.forEach(function(i){
                    var img = new Image();
                    img.onload=function(){
                        console.log(img.src)
                        i.style['background-image'] = img;
                        callback();
                    }
                    img.onerror=function(){
                        callback();
                    }
                    img.src = i.dataset.bgimg;
                });

                function callback(){
                    let sl = document.querySelector('.siteloader');
                    c++;
                    if(c === images.length){
                        sl.style.opacity = 0;
                        setTimeout(()=>sl.style.display = 'none', 1500)
                        interval();
                    }
                }
                function interval(){
                    me.fadeInterval = setInterval(function(){
                        var current = document.querySelector('.logo.current'),
                            next = !!current.nextElementSibling ? current.nextElementSibling : document.querySelector('.js-crossfade .logo');
                        console.log(!!current.nextElementSibling);
                        current.style.opacity = 0;
                        next.style.opacity = 1;
                        current.classList.remove('current')
                        next.classList.add('current')
                    }, 3000);
                }
            }else{
                let sl = document.querySelector('.siteloader');
                if(!!sl){
                    sl.style.opacity = 0;
                    setTimeout(()=>sl.style.display = 'none', 1500)
                }
            }

            // Header Search - Toggle Button
            $('.header-search').on('click', '#header-search-btn', function() {
                if ($(this).hasClass('opened')) {
                    $(this).removeClass('opened');
                } else {
                    $(this).addClass('opened');
                }
                return false;
            });

            $('body').on('click', function() {
                if ($('#header-search-btn').length > 0 && $('#header-search-btn').hasClass('opened')) {
                    $('#header-search-btn').removeClass('opened');
                }
            });
            $('.header-search').on('click', '#header-search', function(event) {
                event.stopPropagation();
            });

            if($('.js-responsiveFullscreenEl').length){
                var el = document.querySelector('.js-responsiveFullscreenEl .inner'),
                    src = el.dataset.bgimg;

                var img = new Image();
                img.onload = function(){
                    el.style['background-image'] = img;
                }

                img.src = src;
            }


            // Top Menu - Toggle Button
            $('.header').on('click', '#mainmenu-btn', function() {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $('.navTrigger', $(this)).removeClass('active');
                    $('#mainmenu-bg').fadeOut();
                    $('body').removeClass('menu-opened');
                } else {
                    var window_height = $(window).height();
                    var items_count = $('#mainmenu > ul > li').length;
                    var items_height = Math.max((window_height - 200) / items_count, 45);
                    var items_height = Math.min(items_height, 85);

                    $(this).addClass('active');
                    $('.navTrigger', $(this)).addClass('active');

                    $('body').addClass('menu-opened');

                    $('#mainmenu > ul > li > a').each(function() {
                        var item_height = $(this).height();
                        var item_margin = items_height - item_height;
                        /*
                        $(this).css('padding-top', Math.round(item_margin / 2));
                        $(this).css('padding-bottom', Math.round(item_margin / 2));
                        */
                    });

                    $('#mainmenu-bg').fadeIn();
                }
                return false;
            });
            $('body').on('click', '#mainmenu-bg', function() {
                $('#mainmenu-btn').removeClass('active');
                $('body').removeClass('menu-opened');
                $('#mainmenu-bg').fadeOut();
                return false;
            });


            // Top Menu Links - Equilize Height
            if ($(window).width() > 751) { // 768
                $('#mainmenu > ul > li.mainmenu-parent')
                    .on("mouseenter", function() {
                        var window_height = $(window).height();
                        var sub_items_count = $(this).find('> ul > li').length;
                        var sub_items_height = Math.max((window_height - 175) / sub_items_count, 40);
                        sub_items_height = Math.min(sub_items_height, 85);

                        $(this).find('> ul').fadeIn(200);

                        $(this).find('> ul > li').each(function() {
                            var item_height = $(this).height();
                            var item_margin = sub_items_height - item_height;
                            $(this).css('margin-bottom', Math.round(item_margin));
                        });
                    })
                    .on("mouseleave", function() {
                        $(this).find('> ul').fadeOut(200);
                    });
            }
            $(window).resize(function() {
                if ($(window).width() > 751) { // 768
                    $('#mainmenu > ul > li.mainmenu-parent')
                        .on("mouseenter", function() {
                            var window_height = $(window).height();
                            var sub_items_count = $(this).find('> ul > li').length;
                            var sub_items_height = Math.max((window_height - 175) / sub_items_count, 40);
                            sub_items_height = Math.min(sub_items_height, 85);

                            $(this).find('> ul > li').each(function() {
                                var item_height = $(this).height();
                                var item_margin = sub_items_height - item_height;
                                $(this).css('margin-bottom', Math.round(item_margin));
                            });

                            $(this).find('> ul').fadeIn(200);
                        })
                        .on("mouseleave", function() {
                            $(this).find('> ul').fadeOut(200);
                        });
                } else {
                    $('#mainmenu > ul > li.mainmenu-parent').off("mouseenter");
                    $('#mainmenu > ul > li.mainmenu-parent').off("mouseleave");
                }
            });
            $('#mainmenu > ul li.mainmenu-parent > a > .fa').on('click', function() {
                if ($(this).parent().parent().hasClass('opened')) {
                    $(this).parent().parent().removeClass('opened');
                    $(this).parent().next('ul').slideUp();
                } else {
                    $(this).parent().next('ul').slideDown();
                    $(this).parent().parent().addClass('opened');
                }
                return false;
            });


            // Homepage Slider
            if ($('#front-slider').length > 0) {
                $('#front-slider').fractionSlider({
                    'fullWidth': true,
                    'controls': true,
                    'pager': false,
                    'responsive': true,
                    'speedIn': 1500,
                    'speedOut': 1500,
                    'slideTransitionSpeed': 1500,
                    'timeout': 1500,
                    'pauseOnHover': false,
                    'dimensions': "1170,750",
                    'startCallback': function(el, currentSlide, lastSlide, step) {
                        $('.topslider .slider .prev').text('Prev').appendTo('.topslider');
                        $('.topslider .slider .next').text('Next').appendTo('.topslider');

                        $('.topslider .responisve-container').css('height', 'auto');
                    },
                    'startNextSlideCallback': function(el, currentSlide, lastSlide, step) {
                        $('.topslider').css('background', $('.topslider .slider .slide').eq(currentSlide).attr('data-bg'));
                    }
                });
            }


            // Instagram Photos (Frontpage) - Masonry Grid
            if ($('#instagram-list').length > 0) {
                $('#instagram-list').masonry({
                    columnWidth: '.instagram-sizer',
                    itemSelector: '.instagram-item',
                    percentPosition: true,
                });
            }


            // About Us - Masonry Grid
            if ($('#about-list').length > 0) {
                $('#about-list').masonry({
                    columnWidth: '.about-i-sizer',
                    itemSelector: '.about-i',
                    percentPosition: true,
                });
            }


            // Fancybox for images
            $('.modal-img').fancybox({
                padding: 0,
                margin: [60, 50, 20, 50],
                helpers: {
                    overlay: {
                        locked: false
                    },
                    thumbs: {
                        width: 60,
                        height: 60
                    }
                },
                tpl: {
                    closeBtn: '<a title="Close" class="fancybox-item fancybox-close modal-close" href="javascript:;"></a>',
                    prev: '<a title="Previous" class="fancybox-nav fancybox-prev modal-prev" href="javascript:;"><span></span></a>',
                    next: '<a title="Next" class="fancybox-nav fancybox-next modal-next" href="javascript:;"><span></span></a>',
                }
            });


            // Modal Window Videos
            $(".about-video").on("click", ".modal-img", function() {
                $.fancybox({
                    'padding': 0,
                    'margin': [60, 20, 20, 20],
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                    'type': 'swf',
                    'swf': {
                        'wmode': 'transparent',
                        'allowfullscreen': 'true'
                    },
                    tpl: {
                        closeBtn: '<a title="Close" class="fancybox-item fancybox-close modal-close" href="javascript:;"></a>',
                        prev: '<a title="Previous" class="fancybox-nav fancybox-prev modal-prev" href="javascript:;"><span></span></a>',
                        next: '<a title="Next" class="fancybox-nav fancybox-next modal-next" href="javascript:;"><span></span></a>',
                    }
                });
                return false;
            });


            // Homepage Posts - Equilize Height
            if ($('.post-front-list').length > 0) {

                if ($(window).width() > 1583) {
                    me.equalize_height('.post-front-list .post-front', 4, 'post-front');
                } else if ($(window).width() > 975) {
                    me.equalize_height('.post-front-list .post-front', 3, 'post-front');
                } else if ($(window).width() > 533) {
                    me.equalize_height('.post-front-list .post-front', 2, 'post-front');
                }
                $(window).resize(function() {
                    if ($(window).width() > 1583) { // 1220
                        me.equalize_height('.post-front-list .post-front', 4, 'post-front');
                    } else if ($(window).width() > 975) { // 768
                        me.equalize_height('.post-front-list .post-front', 3, 'post-front');
                    } else if ($(window).width() > 533) { // 550
                        me.equalize_height('.post-front-list .post-front', 2, 'post-front');
                    } else {
                        $('.post-front-list .post-front').outerHeight('auto');
                    }
                });

            }


            // Related Posts - Equilize Height
            if ($('.post-related-list').length > 0) {

                if ($(window).width() > 1203) {
                    me.equalize_height('.post-related-list .post-related', 4, 'post-related');
                } else if ($(window).width() > 751) {
                    me.equalize_height('.post-related-list .post-related', 3, 'post-related');
                } else if ($(window).width() > 533) {
                    me.equalize_height('.post-related-list .post-related', 2, 'post-related');
                }
                $(window).resize(function() {
                    if ($(window).width() > 1203) { // 1220
                        me.equalize_height('.post-related-list .post-related', 4, 'post-related');
                    } else if ($(window).width() > 751) { // 768
                        me.equalize_height('.post-related-list .post-related', 3, 'post-related');
                    } else if ($(window).width() > 533) { // 550
                        me.equalize_height('.post-related-list .post-related', 2, 'post-related');
                    } else {
                        $('.post-related-list .post-related').outerHeight('auto');
                    }
                });

            }


            // Blog Posts Slider
            if ($('.blog-slider').length > 0) {
                $('.blog-slider').flexslider({
                    animation: "fade",
                    animationSpeed: 500,
                    slideshow: false,
                    animationLoop: false,
                    directionNav: false,
                    smoothHeight: false,
                    controlNav: true,
                });
            }


            // Single Post Slider
            if ($('.post-slider').length > 0) {
                $('.post-slider').flexslider({
                    animation: "fade",
                    animationSpeed: 500,
                    slideshow: false,
                    animationLoop: false,
                    directionNav: false,
                    smoothHeight: false,
                    controlNav: true,
                });
            }


            // Animated ScrollTo
            $('a[data-goto^="#"]').on("click", function() {
                var target = $(this).attr('data-goto');
                $('html, body').animate({ scrollTop: ($(target).offset().top - 30) }, 800);
                return false;
            });


            // Forms Validation
            var filterEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/;
            $('.form-validate').submit(function() {
                var errors = 0;
                $(this).find('[data-required="text"]').each(function() {
                    if ($(this).attr('data-required-email') == 'email') {
                        if (!filterEmail.test($(this).val())) {
                            $(this).addClass("redborder");
                            errors++;
                        } else {
                            $(this).removeClass("redborder");
                        }
                        return;
                    }
                    if ($(this).val() == '') {
                        $(this).addClass('redborder');
                        errors++;
                    } else {
                        $(this).removeClass('redborder');
                    }
                });
                if (errors === 0) {
                    var form1 = $(this);
                    $.ajax({
                        type: "POST",
                        url: 'php/email.php',
                        data: $(this).serialize(),
                        success: function(data) {
                            form1.append('<p class="form-result">Thank you!</p>');
                            $("form").trigger('reset');
                        }
                    });
                }
                return false;
            });
            $('.form-validate').find('[data-required="text"]').blur(function() {
                if ($(this).attr('data-required-email') == 'email' && ($(this).hasClass("redborder"))) {
                    if (filterEmail.test($(this).val()))
                        $(this).removeClass("redborder");
                    return;
                }
                if ($(this).val() != "" && ($(this).hasClass("redborder")))
                    $(this).removeClass("redborder");
            });


            // Homepage Projects Categories - Fixed Block
            if ($('#project-front-sections').length > 0) {
                $(window).scroll(function() {
                    if ($(window).width() > 1123) { // 1140
                        if ($('#project-front-sections').offset().top > $(window).scrollTop()) {
                            $('#project-front-sections .project-front-sections').removeClass('fixed');
                        } else if (($('#project-front-sections').offset().top <= $(window).scrollTop()) && ($('#project-front-list').offset().top + $('#project-front-list').outerHeight()) > ($(window).scrollTop() + $('#project-front-sections .project-front-sections').outerHeight())) {
                            $('#project-front-sections .project-front-sections').addClass('fixed');
                            $('#project-front-sections .project-front-sections').css('top', '0');
                        } else if ($('#project-front-sections .project-front-sections').height() < $('#project-front-list').height()) {
                            $('#project-front-sections .project-front-sections').removeClass('fixed');
                            $('#project-front-sections .project-front-sections').css('top', ($('#project-front-list').outerHeight() - $('#project-front-sections .project-front-sections').outerHeight()) + 'px');
                        } else if ($('#project-front-sections .project-front-sections').height() >= $('#project-front-list').height()) {
                            $('#project-front-sections .project-front-sections').removeClass('fixed');
                            $('#project-front-sections .project-front-sections').css('top', '0px');
                        }
                    }
                });


                var $grid = $('#project-front-list').isotope({
                    itemSelector: '.project-front',
                    layoutMode: 'fitRows'
                });
                $('#project-front-sections').on('click', 'a', function() {
                    var filterValue = $(this).attr('data-section');
                    $grid.isotope({ filter: filterValue });
                });
                $('#project-front-sections').each(function(i, buttonGroup) {
                    var $buttonGroup = $(buttonGroup);
                    $buttonGroup.on('click', 'a', function() {
                        $buttonGroup.find('.active').removeClass('active');
                        $(this).addClass('active');
                        return false;
                    });
                });
            }


            // Blog Posts Categories - Fixed Block
            if ($('#blog-sections').length > 0) {
                $(window).scroll(function() {
                    if ($(window).width() > 1123) { // 1140
                        if ($('#blog-sections').offset().top > $(window).scrollTop()) {
                            $('#blog-sections .blog-sections').removeClass('fixed');
                        } else if (($('#blog-sections').offset().top <= $(window).scrollTop()) && ($('#blog-list').offset().top + $('#blog-list').outerHeight()) > ($(window).scrollTop() + $('#blog-sections .blog-sections').outerHeight())) {
                            $('#blog-sections .blog-sections').addClass('fixed');
                            $('#blog-sections .blog-sections').css('top', '0');
                        } else {
                            $('#blog-sections .blog-sections').removeClass('fixed');
                            $('#blog-sections .blog-sections').css('top', ($('#blog-list').outerHeight() - $('#blog-sections .blog-sections').outerHeight()) + 'px');
                        }
                    }
                });
            }


            // Blog Posts - Show More with AJAX
            $('#blog-showmore').on('click', function(e) {
                $(this).addClass('load');
                $.ajax({
                    type: 'POST',
                    url: 'php/showmore.php',
                    data: { post_type: 'blog' },
                    success: function(data) {
                        var items = $(data);
                        items.addClass('hidden');
                        $('#blog-list').append(items);
                        $('#blog-showmore').removeClass('load');

                        if ($(data).find('.blog-slider').length > 0) {
                            $('.blog-slider').flexslider({
                                animation: "fade",
                                animationSpeed: 500,
                                slideshow: false,
                                animationLoop: false,
                                directionNav: false,
                                smoothHeight: false,
                                controlNav: true,
                            });
                        }

                        setTimeout(function() {
                            items.removeClass('hidden').addClass('show');
                        }, 10);
                    },
                    error: function() {
                        alert('Error');
                    }
                });
                return false;
            });


            // Homepage Projects - Show More with AJAX
            $('#project-front-more').on('click', function(e) {
                $(this).addClass('load');
                $.ajax({
                    type: 'POST',
                    url: 'php/showmore.php',
                    data: { post_type: 'project-front' },
                    success: function(data) {
                        var items = $(data);
                        $('#project-front-list').append(items).each(function() {
                            $('#project-front-list').isotope('reloadItems');
                        });
                        $('#project-front-list').isotope();

                        $('#project-front-more').removeClass('load');
                    },
                    error: function() {
                        alert('Error');
                    }
                });
                return false;
            });

            $('.js-customTeam').on('click', function(e){
                var $el = e.currentTarget,
                    $container = $el.closest('.vc_column-inner '),
                    $detailWrapper = $('.staffDetailWrapper', $container),
                    $contentWrapper = $('.contentWrapper',$el),
                    $append = $('<div class="hidden">'+$contentWrapper.html()+"</div>");
                
                $('.detail', $detailWrapper).html($append.html());
                TweenMax.fromTo($('.staffDetailWrapper'), 0.5, { autoAlpha: 0, left: '100%' }, {autoAlpha: 1, left: 0});
            });

            $('.js-closeStaffDetail').on('click', function(e){
                e.preventDefault();
                TweenMax.fromTo($('.staffDetailWrapper'), 0.5, {autoAlpha: 1, left: 0}, { autoAlpha: 0, left: '100%' });
            });

            $('.mainmenu-parent > a').on('click', function(e){
                e.preventDefault();
                console.log('test', $(window).outerWidth());
                if($(window).outerWidth() < 768){
                    var $ul = $(e.currentTarget).closest('li').find('ul');
                    console.log($ul.length)
                    $ul.css('display', $ul.css('display') === 'none' ? 'block' : 'none');
                }
            });


            me.galleryView = new Views.CycleGalleryView();
            me.galleryView.$el = $('.sl-slider-wrapper', me.$el);
            me.galleryView.render();
        }
    });

    

    Views.ContactsPageView = Views.PageView.extend({
        events: {
            'submit #newsletterForm': 'onNewsletterFormSubmit',
            'submit #contactForm'   : 'onContactFormSubmit'
        },

        emailValidation: function(v){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(v);
        },

        requiredValidation: function(v){
            return !_.isEmpty(v);
        },

        validateForm: function($form){
            var me = this,
                isValid = true;
            console.log($form);
            me.resetValidationMessages($form);
            _.each($('.js-required', $form), function(v, i){
                var $el = $(v),
                    isFieldValid = me.requiredValidation($el.val());
                isValid = isValid && isFieldValid;
                if(!isFieldValid){
                    me.markFieldInvalid($el);
                }
            });

            _.each($('.js-emailValidation', $form), function(v, i){
                var $el = $(v),
                    isFieldValid = me.emailValidation($el.val());
                isValid = isValid && isFieldValid;
                if(!isFieldValid){
                    me.markFieldInvalid($el);
                }
            });

            console.log('onContactFormSubmit');
            return isValid;
        },

        postRender: function(){
            var me = this;
            me._super(Views.PageView, 'postRender', arguments);
            if($("#contacts-map").length){
                function initialize() {
                    var mapOptions = {
                        zoom: 15,
                        scrollwheel: false,
                        center: new google.maps.LatLng(41.053301, 16.7050367)
                    };
                    var map = new google.maps.Map(document.getElementById('contacts-map'),
                        mapOptions);
                    var marker = new google.maps.Marker({
                        position: map.getCenter(),
                        map: map
                    });

                    var infowindow = new google.maps.InfoWindow();

                    google.maps.event.addListener(marker, 'click', function () {

                        infowindow.setContent('<h1>Factor Hill</h1><p>Laboratorio Urbano Rigenera <br /> Via della Resistenza, snc <br /> 70027 - Palo del Colle (BA) <br /> <a href="http://www.rigeneralab.org" target="_blank">www.rigeneralab.org</a></p>');
                        infowindow.open(map, this);
                    });
                }
                google.maps.event.addDomListener(window, 'load', initialize);
            }
        },

        markFieldInvalid: function($field){
            var me = this;
            $field.addClass('error');
        },

        resetValidationMessages: function($form){
            var me = this;
            console.log('resetValidationMessages');
            _.each($('.js-validation', $form), function(v, i){
                $(v).removeClass('error');
            });

            _.each($('.contact-feedback', $form), function(v, i){
                $(v).addClass('hidden');
                $(v).html("");
            });
        },

        onContactFormSubmit: function(e){
            e.preventDefault();
            var me = this,
                $form = $(e.currentTarget),
                isValid = me.validateForm($form);
            console.log(isValid);
            if(isValid){
                $form.addClass('processing').append('<div class="mask"></div>');
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRFToken': $form.find('[name=csrfmiddlewaretoken]').val()
                    },
                    url: $form.data('url'),
                    data: {
                        'subject' : $('input[name=subject]', $form).val(),
                        'sender_email' : $('input[name=sender_email]', $form).val(),
                        'body' : $('textarea[name=body]', $form).val(),
                        'phone': $('input[name=name]', $form).val()
                    },
                    dataType: 'json',
                    complete: function(data, status){
                        var json = (JSON.parse(data.responseText)),
                            msg = "";
                        $form.removeClass('processing');
                        $('.mask', $form).detach();
                        //$("#contact-form fieldset").append('<p class="errorMsg" style="color:red;clear:both;">Attenzione! Errore di comunicazione</p>');
                        if(status==='error'){
                            $(".contacts-form").append('<p class="errorMsg" style="color:red;clear:both;"><small>Attenzione! Errore di comunicazione!</small></p>').removeClass('hidden');
                        }else{
                            $(".contacts-form").append('<p class="errorMsg" style="color:green;clear:both;"><small>Messaggio inviato!</small></p>').removeClass('hidden');
                            $("[type=submit]", $form).detach();
                        }
                    }
                });
            }else{
                return isValid;
            }
        },
        
        onNewsletterFormSubmit: function(e){
            var me = this,
                $form = $(e.currentTarget),
                isValid = me.validateForm($form);
            e.preventDefault();
            if(isValid){
                $form.addClass('processing').append('<div class="mask"></div>');
                $.ajax({
                    url: $form.data('url'),
                    method: 'post',
                    headers: {
                        'X-CSRFToken': $form.find('[name=csrfmiddlewaretoken]').val()
                    },
                    dataType: 'json',
                    data: {
                        "email": $('[name=email]', $form).val(),
                        "mailchimp_list_id": $form.find('input[name=mailchimp_list_id]').val()
                    },
                    complete: function(data, status){
                        var json = (JSON.parse(data.responseText)),
                            msg = "";
                        $form.removeClass('processing');
                        $('.mask', $form).detach();
                        if(typeof(json) === 'string'){
                            json = JSON.parse(json);
                        }
                        if(!json.length){
                            json = [json];
                        }

                        for(var i=0; i<json.length;i++){
                            var obj = json[i];
                            for (var prop in obj) {
                                msg = '<br /> ' + prop + ': ' + obj[prop];
                            }
                        }
                        //$("#contact-form fieldset").append('<p class="errorMsg" style="color:red;clear:both;">Attenzione! Errore di comunicazione</p>');
                        if(status==='error'){
                            $("#subscribeError").append('<p class="errorMsg" style="color:red;clear:both;"><small>Attenzione! Errore di comunicazione: '+msg+'</small></p>').removeClass('hidden');
                        }else{
                            $("#subscribeSuccess").append('<p class="errorMsg" style="color:green;clear:both;"><small>Indirizzo email registrato correttamente</small></p>').removeClass('hidden');
                            $("[type=submit]", $form).detach();
                        }
                    }
                });
                return false;
            }else{
                return isValid;
            }
        }
    });

})();