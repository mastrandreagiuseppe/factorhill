;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    
    Views.PortfolioDetailView = Views.DetailView.extend({
        

        tagName: 'div',

        attributes: function(){
            var me = this;
            return {
                href: me.model.get('absolute_url')
            };
        },

        className: function(){
            var me = this,
                s = 'project-front project-front-width2';
            _.each(me.model.get('categories'), function(c, i){
                s += ' ' + c.slug;
            });
            return s;
        }
        
    });

    Views.PortfolioFilterListView = Views.BaseListView.extend({
        el: '.js-filtersWrapper',

        storeContextName: 'results',
        totalContextName: 'count',
        previousContextName: 'previous',
        nextContextName: 'next',

        preInitialize : function(options){
            var me = this;
            console.log("ListView preInitialize");
            
            me.channel = ChannelBroker.get("AppChannel");
            
            _.each(options, function(v, k){
                me[k] = v;
            });

            me.baseChildrenView = Views.PortfolioFilterView;

            if(me.autoLoad){
                me.collection.fetch();
            }
        }
    });

    Views.PortfolioFilterView = Views.BaseDetailView.extend({
        templateId: '#portfolioCategoryFilterId',

        className: 'category-item',
        tagName: 'li',

        bindEvents: function(){
            var me = this;
            /*me._super(Views.BaseDetailView, 'bindEvents', arguments);*/
            $(me.$el).on('click', function(e){
                e.preventDefault();
                me.onCategoryItemClick();
            });
        },

        onCategoryItemClick: function(){
            var me = this,
                channel = ChannelBroker.get("AppChannel");
            channel.trigger('filter', me);
        },

        attributes: function(){
            var me = this;
            return {
                'data-filter': me.model.get('slug'),
                'href': '#'
            };
        }
    });

    Views.PortfolioPageView = Views.ListView.extend({
        preInitialize : function(options){
            var me = this;
            console.log("PortfolioPageView preInitialize");
            
            me._super(Views.ListView, 'preInitialize', arguments);

            me.filterCollectionConstructor = Collections.Generic.extend({
                url: me.portfolioCategoriesUrl
            });

            me.baseChildrenFilterView = new Views.PortfolioFilterListView({
                collection: new me.filterCollectionConstructor()
            });

            me.baseChildrenFilterView.collection.fetch({
                success: function(){
                    me.baseChildrenFilterView.collection.unshift({ slug: 'project-item', name: 'All' });        
                }
            });
            
            //me.baseChildrenFilterView.render();
        },


        customPostRender: function(){
            $('.header').on('click', '#mainmenu-btn', function() {
                if ($(this).hasClass('opened')) {
                    $(this).removeClass('opened');
                    $('#mainmenu-bg').fadeOut();
                    $('body').removeClass('menu-opened');
                } else {
                    var window_height = $(window).height();
                    var items_count = $('#mainmenu > ul > li').length;
                    var items_height = Math.max((window_height - 200) / items_count, 45);
                    var items_height = Math.min(items_height, 85);

                    $(this).addClass('opened');

                    $('body').addClass('menu-opened');

                    $('#mainmenu > ul > li > a').each(function() {
                        var item_height = $(this).height();
                        var item_margin = items_height - item_height;
                        /*
                        $(this).css('padding-top', Math.round(item_margin / 2));
                        $(this).css('padding-bottom', Math.round(item_margin / 2));
                        */
                    });

                    $('#mainmenu-bg').fadeIn();
                }
                return false;
            });
            $('body').on('click', '#mainmenu-bg', function() {
                $('#mainmenu-btn').removeClass('opened');
                $('body').removeClass('menu-opened');
                $('#mainmenu-bg').fadeOut();
                return false;
            });
            function handleResize(){
                if ($(window).width() > 751) { // 768
                    $('#mainmenu > ul > li.mainmenu-parent')
                        .on("mouseenter", function() {
                            var window_height = $(window).height();
                            var sub_items_count = $(this).find('> ul > li').length;
                            var sub_items_height = Math.max((window_height - 175) / sub_items_count, 40);
                            sub_items_height = Math.min(sub_items_height, 85);

                            $(this).find('> ul > li').each(function() {
                                var item_height = $(this).height();
                                var item_margin = sub_items_height - item_height;
                                $(this).css('margin-bottom', Math.round(item_margin));
                            });

                            $(this).find('> ul').fadeIn(200);
                        })
                        .on("mouseleave", function() {
                            $(this).find('> ul').fadeOut(200);
                        });
                } else {
                    $('#mainmenu > ul > li.mainmenu-parent').off("mouseenter");
                    $('#mainmenu > ul > li.mainmenu-parent').off("mouseleave");
                }
            }
            $(window).resize(function() {
                handleResize();
            });
            handleResize();
            $('#mainmenu > ul li.mainmenu-parent > a > .fa').on('click', function() {
                if ($(this).parent().parent().hasClass('opened')) {
                    $(this).parent().parent().removeClass('opened');
                    $(this).parent().next('ul').slideUp();
                } else {
                    $(this).parent().next('ul').slideDown();
                    $(this).parent().parent().addClass('opened');
                }
                return false;
            });
        },

        bindEvents: function(){
            var me = this;
            me._super(Views.ListView, 'bindEvents', arguments);
            me.listenTo(me.channel, 'filter', me.onFilter);
        },

        renderChildrenViews : function() {
            var me = this;
            var $wrapper;
            if(me.baseChildrenWrapper){
                $wrapper =me.$(me.baseChildrenWrapper);
            }
            else{
                $wrapper = me.$el;
            }
            //var $wrapper = me.$(me.baseChildrenWrapper);//(me.baseChildrenWrapper && me.$(me.baseChildrenWrapper)) || me.$el;
            $wrapper.empty();
            if(me.childrenCollection.length===0){
                $wrapper.append(me.emptyCollection);
            }else{
                var el = '';
                _.each(me.childrenCollection, function(view, index) {
                    if(index%7 === 0){
                        el += '<div class="pageWrapper">';
                    }
                    view.render();
                    el += $(view.el)[0].outerHTML;
                    if(index%7 === 6){
                        el += '</div>';
                    }
                });
                
                $wrapper.append(el);
            }
            return me;
        },

        onFilter: function(view){
            console.log('onFilter');
            var $grid = $('#project-front-list');
            var filterValue = $('a', view.$el).data('section');
            $grid.isotope({ filter: filterValue });
            view.$el.find('.active').removeClass('active');
            $(this).addClass('active');
            return false;
        }
    });

})();