;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    
    
    Views.ListView = Views.BaseListView.extend({
        
        el: '.js-entriesWrapper',

        storeContextName: 'results',
        totalContextName: 'count',
        previousContextName: 'previous',
        nextContextName: 'next',

        preInitialize : function(options){
            var me = this;
            console.log("ListView preInitialize");
            
            me.channel = ChannelBroker.get("AppChannel");
            
            _.each(options, function(v, k){
                me[k] = v;
            });

            me.collectionConstructor = Collections.Generic.extend({
                url: me.listViewUrl
            });

            me.collection = new me.collectionConstructor();

            me.baseChildrenView = _.isFunction(me.detailViewClass) ? me.detailViewClass.extend() : Views.DetailView;

            if(me.autoLoad){
                me.collection.fetch();
            }
        },

        postRender : function() {
            var me = this;
            me.renderChildrenViews();
            me.customPostRender();
        },

        bindEvents : function(options) {
            var me = this;

            me.collection.bind('sync', _.bind(me.render, me));
        },

        customPostRender: function(){
            $('.header').on('click', '#mainmenu-btn', function() {
                if ($(this).hasClass('opened')) {
                    $(this).removeClass('opened');
                    $('#mainmenu-bg').fadeOut();
                    $('body').removeClass('menu-opened');
                } else {
                    var window_height = $(window).height();
                    var items_count = $('#mainmenu > ul > li').length;
                    var items_height = Math.max((window_height - 200) / items_count, 45);
                    var items_height = Math.min(items_height, 85);

                    $(this).addClass('opened');

                    $('body').addClass('menu-opened');

                    $('#mainmenu > ul > li > a').each(function() {
                        var item_height = $(this).height();
                        var item_margin = items_height - item_height;
                        /*
                        $(this).css('padding-top', Math.round(item_margin / 2));
                        $(this).css('padding-bottom', Math.round(item_margin / 2));
                        */
                    });

                    $('#mainmenu-bg').fadeIn();
                }
                return false;
            });
            $('body').on('click', '#mainmenu-bg', function() {
                $('#mainmenu-btn').removeClass('opened');
                $('body').removeClass('menu-opened');
                $('#mainmenu-bg').fadeOut();
                return false;
            });
            function handleResize(){
                if ($(window).width() > 751) { // 768
                    $('#mainmenu > ul > li.mainmenu-parent')
                        .on("mouseenter", function() {
                            var window_height = $(window).height();
                            var sub_items_count = $(this).find('> ul > li').length;
                            var sub_items_height = Math.max((window_height - 175) / sub_items_count, 40);
                            sub_items_height = Math.min(sub_items_height, 85);

                            $(this).find('> ul > li').each(function() {
                                var item_height = $(this).height();
                                var item_margin = sub_items_height - item_height;
                                $(this).css('margin-bottom', Math.round(item_margin));
                            });

                            $(this).find('> ul').fadeIn(200);
                        })
                        .on("mouseleave", function() {
                            $(this).find('> ul').fadeOut(200);
                        });
                } else {
                    $('#mainmenu > ul > li.mainmenu-parent').off("mouseenter");
                    $('#mainmenu > ul > li.mainmenu-parent').off("mouseleave");
                }
            }
            $(window).resize(function() {
                handleResize();
            });
            handleResize();
            $('#mainmenu > ul li.mainmenu-parent > a > .fa').on('click', function() {
                if ($(this).parent().parent().hasClass('opened')) {
                    $(this).parent().parent().removeClass('opened');
                    $(this).parent().next('ul').slideUp();
                } else {
                    $(this).parent().next('ul').slideDown();
                    $(this).parent().parent().addClass('opened');
                }
                return false;
            });
        }

    });

})();