;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    

    Objects.conf = {};


    Objects.conf['errorMessages'] = {
        "A valid integer is required.": "Inserire un numero intero valido.",
        "This field may not be blank.": "Campo obbligatorio.",
        "A valid number is required.": "Inserire un numero valido.",
        "This field must be unique.": "Questo valore è già presente a db, specificare un altro valore."
    };

})();