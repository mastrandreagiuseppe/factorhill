;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    
    Views.VideoListView = Views.ListView.extend({
        postRender: function(){
            var me = this;
            me.renderChildrenViews();
            me.handleVideoJs();
        },

        bindEvents: function(){
            var me = this;

            me._super(Views.ListView, 'bindEvents', arguments);

            me.listenTo(me.channel, 'playNewVideo', me.onPlayNewVideo );
        },

        onPlayNewVideo: function(model){
            var me = this,
                url = model.get('link'),
                sources = [
                    { "type": (url.indexOf('yout') > 1) ? "video/youtube" : "video/vimeo", "src": url }
                ];
            me.player.pause();
            me.player.src(sources);
            me.player.load();
            me.player.play();
        },

        handleVideoJs: function(){
            var me = this,
                options = {
                    controls: true
                };

            var s = me.childrenCollection.length ? me.childrenCollection[0] : null,
                videoId = s ? s.getVideoId() : '';
            


            if(!!videoId){
                var url = s.model.get('link');
                options['sources'] = [
                     { "type": (url.indexOf('yout') > 1) ? "video/youtube" : "video/vimeo", "src": url }
                ];
            }

            if(s.model.get('link').indexOf('yout') > 1){
                options['techOrder'] = ['youtube'];
            }else{
                options['techOrder'] = ['vimeo'];
            }


            if($('#player').length && $('#player')[0].tagName.toLowerCase() === 'video'){
                me.player = videojs('player', options, function onPlayerReady() {
                    videojs.log('Your player is ready!');

                    // In this context, `this` is the player that was created by Video.js.
                    // this.play();

                    // How about an event listener?
                    this.on('ended', function() {
                      videojs.log('Awww...over so soon?!');
                    });
                });
            }
        }
    });


    Views.VideoDetailView = Views.DetailView.extend({
        
        events: {
            'click .js-playVideo': 'onJsPlayVideo'
        },

        onJsPlayVideo: function(e){
            e.preventDefault();
            var me = this;
            me.channel.trigger('playNewVideo', me.model);
        },

        afterThumbLoad: function(img){
            var me = this;
            $('img', me.$el).attr('src', img.src);
            $('img', me.$el).removeClass('imageReplacer');
        },

        preloadImage: function(){
            var me = this,
                img = new Image(),
                startIndex, videoId, thumbUrl;
            img.onload = function(){
                me.afterThumbLoad(img);
            };

            if(!me.model.get('cover_img')){
                var link = me.model.get('link'),
                    videoId = me.getVideoId(link);

                if(link.indexOf('you') > -1){
                    startIndex = ( link.indexOf('?v=') > -1) ? link.indexOf('?v=')+3 : link.indexOf('.be/')+4;
                    videoId = link.substring( startIndex, startIndex + 11);
                    thumbUrl = 'https://img.youtube.com/vi/'+ videoId +'/hqdefault.jpg';
                    img.src = thumbUrl;
                }else if(link.indexOf('vimeo') > -1 ){
                    startIndex = link.indexOf('.com/')+5;
                    videoId = link.substring( startIndex );
                    $.ajax({
                        method: 'get',
                        url: 'http://vimeo.com/api/v2/video/'+videoId+'.json'
                    }).success(function(response, status){
                        thumbUrl = response[0].thumbnail_large;
                        img.src = thumbUrl;
                    }).fail(function(){
                        console.log('failed to load thumbnail');
                    });
                }
            }else{
                img.src = me.model.get('cover_img').image;
            }
        },

        postRender: function(){
            var me = this;
            me._super(Views.DetailView, 'postRender', arguments);
        },

        getVideoId: function(link){
            var me = this,
                link = _.isString(link) ? link : me.model.get('link'),
                startIndex, videoId;
            if(link.indexOf('you') > -1){
                startIndex = ( link.indexOf('?v=') > -1) ? link.indexOf('?v=')+3 : link.indexOf('.be/')+4;
                videoId = link.substring( startIndex, startIndex + 11);
            }else if(link.indexOf('vimeo') > -1 ){
                startIndex = link.indexOf('.com/')+5;
                videoId = link.substring( startIndex );
            }

            return videoId;
        }

    });

})();