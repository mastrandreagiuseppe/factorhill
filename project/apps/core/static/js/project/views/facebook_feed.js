(function() {
    var Models = namespace('Project.Models'),
        Collections = namespace('Project.Collections'),
        Routers = namespace('Project.Routers'),
        Urls = namespace('Project.Urls'),
        Vars = namespace('Project.Vars'),
        Objects = namespace('Project.Objects'),
        Views = namespace('Project.Views');


    Views.FacebookFeedDetailView = Views.DetailView.extend({
        
        templateId: '#facebookEntryTpl',

        tagName: 'li',

        attributes: function(){
            var me = this;
            return {
                href: me.model.get('absolute_url')
            };
        },

        className: function(){
            var me = this,
                s = 'instagram-item';
            return s;
        },

        preInitialize : function(options){
            var me = this;
            console.log("DetailView preInitialize");

            me.channel = ChannelBroker.get("AppChannel");

            _.each(options, function(v, k){
                me[k] = v;
            });

            if(!!me.modelName){
                me.model = new me.modelName();
            }
        },


        preloadImage: function(){
            var me = this;
            if(!!me.model.get('full_picture')){
                var img = new Image();
                img.onload = function(){
                    $('.imgBg', me.$el).css('background-image', 'url('+img.src+')');
                    $('.imgBg', me.$el).css('background-size', 'cover');
                };

                img.src = me.model.get('full_picture');
            }
        }
    });

    Views.FacebookFeedView = Views.BaseListView.extend({

        el: '.js-facebookFeed',

        storeContextName: 'data',
        totalContextName: 'count',
        previousContextName: 'paging.previous',
        nextContextName: 'paging.next',

        templateId: '#facbookFeedTpl',

        tagName: 'ul',

        className: 'instagram-list',

        preInitialize: function(options){
            var me = this;

            for(var k in options){
                console.log(k, options[k]);
                me[k] = options[k];
            }
        },

        postInitialize: function(){
            var me = this;
            me.initFb();
            me.baseChildrenView = Views.FacebookFeedDetailView;
        },

        loadCallback: function(){
            var me = this;

            FB.api(
                "/factorhill/feed",
                {
                    access_token: me.accessToken,
                    fields: ['full_picture', 'description', 'name', 'permalink_url', 'message', 'reactions', 'shares' ]
                },
                function (response) {
                  if (response && !response.error) {
                    me.collection = new Collections.Generic();
                    me.collection.add(response.data);
                    me.render();
                  }
                }
            );
        },

        initFb: function(){
            var me = this;
            window.fbAsyncInit = function() {
                FB.init({
                    appId            : me.appId,
                    autoLogAppEvents : true,
                    xfbml            : true,
                    version          : 'v2.9'
                });
                FB.AppEvents.logPageView();
                me.loadCallback();
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        },

        postRender: function() {
            console.log('postRender');
            var me = this;
            me.renderChildrenViews();
        }
    });

})();