/*
 Example:
 - Listen:
   var ch = ChannelBroker.get('myAwesomeChannel');
   me.listenTo(ch, 'myCustomEvent', myCallback);
 - Trigger  
   var ch = ChannelBroker.get('myAwesomeChannel');
   ch.trigger('myCustomEvent', data1, data2, ecc...);
 */
!function(){
	
	var Channel = function() {
	};

	_.extend(Channel.prototype, Backbone.Events);
	
	Channel.extend = Backbone.View.extend;
	
	var channels = {
			
	};
	
	window.ChannelBroker = {
		
		get: function(channel_name){
			if(!channels[channel_name]){
				channels[channel_name] = new Channel();
				channels[channel_name].name = channel_name;
			}
			return channels[channel_name];
		}
	};
	
}();