
function ShareBtns(obj){
    this.btns = obj.btns;
    this.title = !!obj.title ? obj.title : "Default title";
    this.description = !!obj.description ? obj.description : "Default description";
    this.url = !!obj.url ? obj.url : "Default url";
    this.emailBody = !!obj.emailBody ? obj.emailBody : "Default emailBody";
    this.init();
}

ShareBtns.prototype.init = function(){
    var me = this;

    me.btns.forEach(function(btn){
        var b = document.querySelector(btn);
        if(!!b){
            b.addEventListener('click', function(e){
                e.preventDefault();
                switch(b.dataset.social){
                    case 'facebook':
                    me.fbShare(b);
                    break;
                    case 'twitter':
                    me.twitterShare(b);
                    break;
                    case 'mail':
                    me.mailShare(b);
                    break;
                    case 'linkedin':
                    me.linkedinShare(b);
                    break;
                    default:
                        console.log('Social', b.dataset.social, 'still not managed');
                    break;
                }
            });
        }
    });
}

ShareBtns.prototype.fbShare = function(btn){
    var me = this,
        text = encodeURIComponent(me.description),
        shareUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + me.url + '&title=' + me.title;
    
    var win = window.open(shareUrl, 'ShareOnFb', me.getWindowOptions());
    win.opener = null; // 2

}

ShareBtns.prototype.twitterShare = function(btn){
    var me = this,
        text = encodeURIComponent(me.description),
        shareUrl = 'https://twitter.com/intent/tweet?url=' + me.url + '&text=' + text;
    
    var win = window.open(shareUrl, 'ShareOnTwitter', me.getWindowOptions());
    win.opener = null; // 2
}

ShareBtns.prototype.linkedinShare = function(btn){
    var me = this,
        text = encodeURIComponent(me.description),
        shareUrl = 'https://www.linkedin.com/shareArticle?mini=true&url=' + me.url + '&title='+ me.title +'&summary=' + text;
    
    var win = window.open(shareUrl, 'ShareOnTwitter', me.getWindowOptions());
    win.opener = null; // 2
}

ShareBtns.prototype.mailShare = function(btn){
    var me = this;
    const el = document.createElement('a');
    el.setAttribute('href', 'mailto:?body=' + btn.dataset.emailbody);
    document.body.appendChild(el);
    el.click();
    document.body.removeChild(el);
}

ShareBtns.prototype.getWindowOptions = function() {
  var width = 500;
  var height = 350;
  var left = (window.innerWidth / 2) - (width / 2);
  var top = (window.innerHeight / 2) - (height / 2);

  return [
    'resizable,scrollbars,status',
    'height=' + height,
    'width=' + width,
    'left=' + left,
    'top=' + top,
  ].join();
};