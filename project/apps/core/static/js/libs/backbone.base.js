/**
 * FHCoreLibs - fh_backbone_base.js 0.3.3
 * (c) 2013 Frankhood Business Solution
 *
 */
var namespace = function(space, base, firstsMustExists) {
    /*
     Create a new namespace
     */
    var a = [space], o = null, i, j, d;
    for ( i = 0; i < a.length; i ++) {
        d = a[i].split(".");
        o = base && ( ( _.isString(base) && namespace(base) ) || base ) || window;
        for ( j = 0; j < d.length; j ++) {
            if (firstsMustExists && j < d.length - 1 && o[d[j]] === undefined) {
                throw d[j] + " must exists in " + space;
            }
            if(!o[d[j]]){
                o[d[j]] = {};
            }
            o = o[d[j]];
        }
    }
    return o;
};

/*
 * reference http://tech.pro/tutorial/2011/functional-javascript-part-4-function-currying
 * this variant of currying supports also holes in the passed arguments
 * var f = curry(function(a, b, c) { return [a, b, c]; });
 * f("a","b","c");
 * f("a")("b")("c");
 * f("a", _, "c")("b");
 * f( _, "b")("a", "c");
 * => ["a", "b", "c"]
 */
var curry = function(fn, length, args, holes) {
    length = length || fn.length;
    args = args || [];
    holes = holes || [];
    return function(){
        var _args = args.slice(0),
            _holes = holes.slice(0),
            argStart = _args.length,
            holeStart = _holes.length,
            arg, i;
        for(i = 0; i < arguments.length; i++) {
            arg = arguments[i];
            if(arg === _ && holeStart) {
                holeStart--;
                _holes.push(_holes.shift()); // move hole from beginning to end
            } else if (arg === _) {
                _holes.push(argStart + i); // the position of the hole.
            } else if (holeStart) {
                holeStart--;
                _args.splice(_holes.shift(), 0, arg); // insert arg at index of hole
            } else {
                _args.push(arg);
            }
        }
        if(_args.length < length) {
            return curry.call(this, fn, length, _args, _holes);
        } else {
            return fn.apply(this, _args);
        }
    };
};

var superClass = function(base, scope, method, args) {
    if (arguments.length > 4) {
        var slice = Function.call.bind(Array.prototype.slice);
        args = slice(arguments, 3);
    }
    return base.prototype[method].apply(scope, args || []);
};

function createBaseView(ns) {
    var viewOptions = ['model', 'collection', 'el', 'id', 'attributes', 'className', 'tagName', 'events', // BASE VIEW OPTIONS
    'templateId', 'elCss'];

    var Dispatchers     = namespace( 'Dispatchers', ns),
        Models          = namespace( 'Models', ns),
        Collections     = namespace( 'Collections', ns),
        Objects         = namespace( 'Objects', ns),
        Vars            = namespace( 'Vars', ns),
        Views           = namespace( 'Views', ns);

    var BaseView = Backbone.View.extend({

        templateId : null,
        elCss : null,

        _configure : function(options) {
            if (this.options)
                options = _.extend({}, _.result(this, 'options'), options);
            _.extend(this, _.pick(options, viewOptions));
            this.options = options;
        },

        _super : function(base, method, args) {
            // Call "superclass" method (useless?)
            return superClass(base, this, method, args);
        },

        initialize : function(options) {
            var me = this;
            me.preInitialize(options);

            me._super(Backbone.View, 'initialize', arguments);
            me.bindEvents(options);

            me.postInitialize(options);
        },

        postInitialize : function(options) {
            // Implement in concrete class
        },

        preInitialize : function(options) {
            // Implement in concrete class
        },

        bindEvents : function(options) {
            // Implement in concrete class
        },

        getTemplate : function() {
            var me = this;
            if (me.template) {
                if (_.isFunction(me.template)) {
                    return me.template.apply(me);
                }
                return me.template;
            }
            if (me.templateId) {
                var templateId = me.templateId;
                if (_.isFunction(me.templateId)) {
                    templateId = me.templateId.apply(me);
                }
                return _.template($(templateId).html());
            }

            return null;
            //throw "Must defined template or templateId";
        },

        getContext : function() {
            return {};
        },

        preRender : function() {
            return this;
        },

        postRender : function() {
            return this;
        },

        render : function() {
            var me = this;
            var tmpl = me.getTemplate();
            var context = me.getContext();
            //
            me.preRender();
            //
            if(tmpl){
                me.$el.html(tmpl(context));
            }
            if (me.elCss) {
                var elCss = me.elCss;
                if (_.isFunction(elCss)) {
                    elCss = elCss.apply(this);
                }
                me.$el.css(elCss);
            }
            //
            me.postRender();
            //
            return me;
        },


        detach : function() {
            this.$el.detach();
            return this;
        }
    });

    var BaseDetailView = BaseView.extend({

        bindEvents : function(options) {
            var me = this;
            me.listenTo(me.model, 'all', me.handlerAll);
        },

        handlerAll : function() {
            this.render();
        },

        getContext : function() {
            var me = this;
            return {
                model : me.model && me.model.toJSON()
            };
        }
    });

    var BaseListView = BaseView.extend({

        baseChildrenView : BaseDetailView,
        baseChildrenCollection : null,
        baseChildrenWrapper : null,

        bindEvents : function(options) {
            var me = this;
            me.listenTo(me.collection, 'all', me.handlerAll);
        },

        handlerAll : function() {
            this.render();
        },

        preRender : function() {
            var me = this;
            me.createChildrenViews();
            return me;
        },

        postRender : function() {
            return this.renderChildrenViews();
        },

        createChildrenViews : function() {
            var me = this,
                views = [];
            var cc = me.baseChildrenCollection || Backbone.ViewsCollection;
            if (!me.childrenCollection) {
                me.childrenCollection = new cc();
            }
            me.collection.each(function(obj) {
                views.push(me.createChildView(obj));
            });

            me.childrenCollection = views;
            return me;
        },

        createChildView : function(obj){
            var me = this;

            return new me.baseChildrenView({
                model : obj
            });
        },

        renderChildrenViews : function() {
            var me = this;
            var $wrapper;
            console.log(me.$el);
            if(me.baseChildrenWrapper){
                $wrapper =me.$(me.baseChildrenWrapper);
            }
            else{
                $wrapper = me.$el;
            }
            //var $wrapper = me.$(me.baseChildrenWrapper);//(me.baseChildrenWrapper && me.$(me.baseChildrenWrapper)) || me.$el;
            $wrapper.empty();
            if(me.childrenCollection.length===0){
                $wrapper.append(me.emptyCollection);
            }else{
                _.each(me.childrenCollection, function(view) {
                    $wrapper.append(view.render().$el);
                });
            }
            return me;
        },

        emptyCollection : function(){
             var me = this;

             return "";
        },

        removeChildrenViews : function() {
            var me = this;
            if (me.childrenCollection) {
                me.childrenCollection.each(function(view) {
                    view.remove();
                });
                me.childrenCollection.reset();
            }
            return me;
        },

        unbindCustomEvents : function() {
            return this;
        },

        remove : function() {
            var me = this;
            me.unbindCustomEvents();
            me.removeChildrenViews();
            return me._super(BaseView, 'remove', arguments);
        },

        getContext : function() {
            var me = this;
            return {
                collection : me.collection && me.collection.toJSON()
            };
        }
    });

    Views.BaseView = BaseView;
    Views.BaseDetailView = BaseDetailView;
    Views.BaseListView = BaseListView;

    /* **** MODEL ***** */

    var BaseModel = Backbone.Model.extend({

        contextObjectName : null,

        toJSON : function(options) {
            var me = this;
            var attrs = _.clone(me.attributes);
            _.each(_.keys(attrs), function(key) {
                if (attrs[key] && _.isFunction(attrs[key].toJSON)) {
                    attrs[key] = attrs[key].toJSON();
                }
            });
            return attrs;
        },
        parse : function( data ) {
            var me = this,
                res;
            if(me.contextObjectName){
                res = (data && (typeof(data[me.contextObjectName])!=="undefined") && ( _.isArray( data[me.contextObjectName] ) ? (data[me.contextObjectName].length>0 ? data[me.contextObjectName][0] : {"_not_exists": true} ): data[me.contextObjectName] ) ) || data;
            }else{
                res = data;
            }
            return res;
        }
    });

    var BaseCollection = Backbone.Collection.extend({

        contextObjectName : null,
        contextObjectMeta : null,
        contextObjectMetaValues : null,

        initialize:function(o){
            var me = this;

            if(o && o.url){
                me.url = o.url;
            }
        },

        parse : function(data){
            var me = this;
            if(me.contextObjectMeta){
                if(typeof me.contextObjectMeta === 'object' && typeof me.contextObjectMeta.slice === 'function'){
                    me.contextObjectMetaValues = {};
                    _.each(me.contextObjectMeta, function(element, index, list){
                        me.contextObjectMetaValues[element] = data[element];
                    });
                }else{
                    throw "contextObjectMeta non risulta essere un array";
                }
            }
            if(me.contextObjectName){
                return data && data[me.contextObjectName];
            }else{
                return data;
            }
        }

    });

    Models.BaseModel = BaseModel;
    Collections.BaseCollection = BaseCollection;
}
