/*
 * Orderable.js 
 * For OrderableModelMixin
 * 
 * Author:
 * Frankhood S.r.l.
 * 
 * Date:
 * May 14, 2012
 */

compare_asc = function(a,b){
	return a-b;
}

compare_desc = function(a,b){
	return b-a;
}

copy_array = function(old_array){
	new_array=[];
	for(item in old_array){
		new_array[item]=old_array[item];
	}
	return new_array;
}

compare_arrays = function(array_1, array_2){
	/*
	 * Restituisce true se gli array:
	 * cond 1. hanno lunghezza uguale
	 * cond 2. hanno gli stessi elementi nelle stesse posizioni
	 */
	
	array_equals= true;
	
	//cond 1.
	if(array_1.length != array_2.length){
		array_equals= false;
	}else{
		//cond 2.
		for(var i=0;i<array_1.length;i++){
			for(var j=0;j<array_2.length;j++){
				if(array_1[i]==array_2[j]){
					if(i!=j){
			   			array_equals= false;
					}
				}
			}
		}
	}
	return array_equals;
}

sort_and_compare_array= function(_array_){
	/*
	 * Restituisce true se :
	 * _array_ == _array_(ordinato)
	 * where:
	 * - _array_ = array da ordinare
	 * n.b.  _order_asc_ = boolean che indica se l'ordine, se c'è!!,  è asc(true) o desc(false)
	 */
	
	var _is_ordered = false,
		_order_asc = true;
	
	if (_array_.length > 1) {
		var _array_copied = copy_array(_array_);
		if (_array_copied[0] > _array_copied[1]) {
			_order_asc = false;
		}
		
		if (_order_asc == true) {
			_array_copied.sort(compare_asc);
		}
		else {
			_array_copied.sort(compare_desc);
		}
		_is_ordered = compare_arrays(_array_copied, _array_);
	}
	return {
			'_can_order': _is_ordered,
			'_ascendent_order': _order_asc
		};
}


orderFields = function(){
	
	/*
	 * VARIABILI
	 */
	
	var _possible_order_fields=['order','sort_num'],
	    _order_field='',
	    $container = $("#result_list tbody"),
		placeholder_class = ".ui-sortable-placeholder",
		ajax_url = window.location.origin+"/api/save-orderable/",
		_pathSplitted = window.location.pathname.split("/"),
		_app_label = _pathSplitted[_pathSplitted.length - 3],
		_model = _pathSplitted[_pathSplitted.length - 2];
	
	/*
	 * VARIABILI DI CONTROLLO
	 */
	
	var _first_time=true;
		
	/*
	 * METODI 
	 */
	
	var TakeOrderField = function(){
		order_field = '';
		for (var i = 0; i < _possible_order_fields.length; i++) {
			if($container.find('input[name$="'+_possible_order_fields[i]+'"]').length>0){
				order_field=_possible_order_fields[i];
			}
		}
		return order_field;
	}
	
	var ControlOrdering = function(){
		/*
		 * Metodo per controllare se la change list 
		 * è ordinata per il field di ordinamento 
		 * e se si se è ordinata in modo crescente oppure no
		 * "sort_and_compare_array" returns:
		 * - _can_order :            è ordinato [boolean]
		 * - _ascendent_order:       l'ordine è ascendente [boolean]
		 */
		var _ordering_array = [];
		$container.find('input[name$="'+_order_field+'"]').each(function(e){
			_ordering_array.push(parseInt($(this).val()));
		});
		return sort_and_compare_array(_ordering_array);
	}
	
	var OrderAndUpdateAjax = function(_id_ , _order_){
		/*
		 * Chiamata Ajax nella quale vado a indicare id dell'item del quale è cambiata la posizione e la posizione stessa
		 * La vista lato Server restituisce un'oggetto Json lista di oggetti del tipo:
		 * [{'id': id,'order':order}]
		 * Per ogni oggetto vado a modificare l'order nel campo apposito della change_list
		 */
		
		$.ajax({
			type: 'get',
			url: ajax_url,
			data: {
				'order_field': _order_field,
				'app_label': _app_label,
				'app_model': _model,
				'obj_id': _id_,
				'new_order': _order_,
				'redirect_url': window.location.host + window.location.pathname
			},
			success: function(remote_data){
				for (i = 0; i < remote_data.length; i++) {
					$item = $container.find('input.action-select[value="' + remote_data[i]['id'] + '"]');  //$item = $container.find('a[href="' + remote_data[i]['id'] + '/"]');
					$item.parent().siblings('td').find('input[name$=' + _order_field + ']').val(remote_data[i][_order_field]);
				}
			},
			complete: function(jqXHR, textStatus){
			}
		});
	}			
	
	var RemovePlaceHolder = function(_ui_){
		/*
		 * Distruggo Sortable e se mi è stato passto un item vado a rimuovere il placeholder e ad azzerare lo stile modificato da jquery-ui
		 */
		if(_ui_){
			_ui_.item.attr("style", '');
			$(placeholder_class).remove();
		}
	}
	
	var ActiveManualOrder = function(){
		/*
		 * per ogni elemento di tipo posizione:
		 * 1. abilito la modifica manuale
		 * 2. attacco un listener che chiama per quell'elemento OrderAndUpdateAjax 
		 */
		$container.find('input[name$="'+_order_field+'"]').each(function(e){
			$(this).attr('readonly',null);
			$(this).css('background-color','white');
			$item= $(this);
			$item.bind('change',function(e){
				var _sorting_element_id = $(this).parent().siblings().find('input.action-select').attr("value");
				var _$current_order = $(this);
				OrderAndUpdateAjax(_sorting_element_id, _$current_order.val() );
			});
		});
	}
	
	var ModifyOrder = function(sort_ui, order_asc){
		/*
		 * sort_ui rappresenta l'item appena spostato...
		 * adesso vado ad inserire la posizione che ci si aspetta di avere dopo lo spostamento, 
		 * e cioè(in caso di order crescente) quella dell'item successivo se vado indietro
		 * o del precedente se vado in avanti..(o viceversa in caso di ordine decrescente)
		 * La nuova Posizione viene poi restituita
		 * 
		 * N.B. Da Rivedere per il carico computazionale della ricerca all'interno di una stringa
		 * 
		 */
		var _$current_order = sort_ui.item.find('input[name$=' + _order_field + ']');
		
		if (order_asc) {
			var _$prev_order = sort_ui.item.prev('tr').find('input[name$=' + _order_field + ']');
			var _$next_order = sort_ui.item.next('tr').find('input[name$=' + _order_field + ']');
		}else{
			//se l'ordine è decrescente basta invertire il precedente con il successivo:
			var _$next_order = sort_ui.item.prev('tr').find('input[name$='+_order_field+']');
			var _$prev_order = sort_ui.item.next('tr').find('input[name$='+_order_field+']');
		}
		if (_$next_order.length) {
			// guardo se ho spostato l'item indietro
			if (parseInt(_$current_order.val()) > parseInt(_$next_order.val())) {
				var ar = sort_ui.item.nextAll();

				for (var i = 0; i<ar.length; i++){
					var tr = $(ar[i]),
						el = $('input[name$=' + _order_field + ']', tr),
						curVal = parseInt(el.val());
					console.log(curVal+1);
					el.val(curVal+1);
				}
				_$current_order.val(_$next_order.val()-1);

			}//o in avanti
			else {
				var ar = sort_ui.item.prevAll();

				for (var i = ar.length-1; i >= 0; i--){
					var tr = $(ar[i]),
						el = $('input[name$=' + _order_field + ']', tr),
						curVal = parseInt(el.val());
					el.val(curVal-1);
				}
				
				if (_$prev_order.length) {
					_$current_order.val(parseInt(_$prev_order.val())+1);
				}
			}
		}
		else 
			if (_$prev_order.length) {
				//sto spostando in ultima posizione
				var ar = sort_ui.item.prevAll();

				for (var i = ar.length-1; i >= 0; i--){
					var tr = $(ar[i]),
						el = $('input[name$=' + _order_field + ']', tr),
						curVal = parseInt(el.val());
					el.val(curVal-1);
				}
				_$current_order.val(parseInt(_$prev_order.val())+1);
			}
		return _$current_order.val();	
	}
	
	var ActiveAutomaticOrder = function(sorting_ui, order_asc){
		var _sorting_element_id = sorting_ui.item.find('input.action-select').attr("value"); //var _sorting_element_id = sort_ui.item.find('a').attr("href").split("/")[0];
		var _currentPosition = ModifyOrder(sorting_ui, order_asc);
		OrderAndUpdateAjax(_sorting_element_id, _currentPosition);
	}
	
	// controllo che ci sia un field di ordinamento e nel caso lo assegno alla variabile _order_field
	var Init = function(){
		_order_field = TakeOrderField();
		if ($container.length>0 && _order_field.length > 0) {
			$container.sortable();
			$container.find('input[name$="' + _order_field + '"]').each(function(e){
				/*$(this).attr('readonly', 'readonly');*/
				$(this).css('background-color','#EBEBE4');
			});
			var ctrl_Obj = ControlOrdering();
			$container.bind("sortstart", function(event, ui){
				if (_first_time) {
					if (!ctrl_Obj['_can_order']) {
						//alert("Se vuoi Ordinare con il Drag & Drop devi prima ordinare gli item per POSIZIONE..\nPuoi altrimenti adesso modifcare a mano la posizione");
						alert("If you want to sort with Drag&Drop you have to sort items for order fields..\nYou can now modify manually item's position ");
						ActiveManualOrder();
						RemovePlaceHolder(ui);
						$container.sortable("destroy");
					}
					_first_time = false;
				}
			});
			if (ctrl_Obj['_can_order']) {
				$container.bind("sortupdate", function(event, ui){
					ActiveAutomaticOrder(ui, ctrl_Obj['_ascendent_order']);
				});
			}
		}
	}
	
	return Init();
}

$(function(){
	orderFields();
});




/*
 * Div class="inline-group" -> sortable
 *   div class="inline-related"
 *      fieldset div .order
 * 
 * 
 * per salvare in un array:
 * order_array = $("#result_list tbody").find('input[name$=order]');
 * for(i=0; i<order_array.length; i++){
 * 		console.log($(order_array[i]).attr('value')); //oppure
 * 		console.log($(order_array.eq(i).attr('value'));
 * }
 * array2 = $("#result_list tbody tr");
 * for(i=0; i<array2.length; i++){
 * 		console.log($(array2[i]).attr('class')):
 * }
 * 
 * $("#result_list tbody").find('input[name$=order]').bind('change',function(e){console.log("ciaoo");})
 * 
 * Ordered_Object = function(){
 * 		
 * 		
 * 
 * 
 * }
 * 
 * 
 */





/*
$container = $("#result_list tbody");
_order_field = 'order'
remote_data=[{id:3, order:7},{id:6, order:8},{id:2, order:22}]
for(i=0; i<remote_data.length; i++){
							$item = $container.find('a[href="'+remote_data[i]['id']+'/"]');
							console.log($item);
							console.log('id ='+remote_data[i]['id']);
							console.log('_order_field = '+_order_field);
							console.log('order client ='+$item.parent().siblings('td' > 'input[name$='+_order_field+']').val());
							console.log('order server ='+remote_data[i][_order_field]);
							$item.parent().siblings('td' > 'input[name$='+_order_field+']').val(remote_data[i][_order_field]);
							console.log('order client ='+$item.parent().siblings('td' > 'input[name$='+_order_field+']').val());
							
						}
*/