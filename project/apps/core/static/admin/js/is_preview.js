doPreview = function(control, topic_n, topic_v) {
	
	hrefPath = window.location.href;
	basePath = hrefPath.substring(0,hrefPath.length-window.location.search.length);
	splittedPath = basePath.split('/');
	id_preview = splittedPath.pop();
	new_id_preview = parseInt(control);
	if(!id_preview){id_preview = splittedPath.pop();}
	if(basePath.substring(basePath.length-1,basePath.length)!='/'){
		basePath+='/';
	}
	topic_slug='';
	if(topic_n&&topic_v){
		topic_slug = '&'+topic_n+'='+topic_v;
	}
	
	new_window = window.open(basePath.substring(0,basePath.length-(id_preview.length+1)) +'preview/' + parseInt(new_id_preview) + '/' + '?is_preview=True'+topic_slug, "Preview_Window");
	new_window.focus();
	
}
$(function(){
			
	/*
	 * PLUGIN X QUERYSTRING
	 */
		(function($){
			$.QueryString = (function(a){
				if (a == "") 
					return {};
				var b = {};
				for (var i = 0; i < a.length; ++i) {
					var p = a[i].split('=');
					if (p.length != 2) 
						continue;
					b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
				}
				return b;
			})(window.location.search.substr(1).split('&'))
		})(jQuery);
		
		set_QueryString = function(a){
			if (a == "") 
				return {};
			var b = {};
			a = a.split('&');
			for (var i = 0; i < a.length; ++i) {
				var p = a[i].split('=');
				if (p.length != 2) 
					continue;
				b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
			}
			return b;
		}
		
	

	
	var is_preview = $.QueryString['is_preview'];
	var topic_name = $.QueryString['topic_name'];
	var topic_value = $.QueryString['topic_value'];
	
	if (is_preview) {
		if(topic_name&&topic_value){ 
			doPreview(is_preview,topic_name,topic_value);
		}else{
			doPreview(is_preview,false,false);
		}
		
	}
});